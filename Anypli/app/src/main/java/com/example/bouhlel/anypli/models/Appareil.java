package com.example.bouhlel.anypli.models;

import java.io.Serializable;

/**
 * Created by bouhlel on 19/07/17.
 */

public class Appareil implements Serializable {

    private int appareil_id;
    private String categorie;
    private String marque;
    private String reference;
    private String microprocesseur;
    private int cache;
    private int ram;
    private int disqueDur;
    private String os;
    private String qrcode;
    private int disponible;
    private String dateAjout;

    public Appareil() {
    }

    public String getCategorie() {
        return categorie;
    }

    public int getAppareil_id() {
        return appareil_id;
    }

    public void setAppareil_id(int appareil_id) {
        this.appareil_id = appareil_id;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getMicroprocesseur() {
        return microprocesseur;
    }

    public void setMicroprocesseur(String microprocesseur) {
        this.microprocesseur = microprocesseur;
    }

    public int getCache() {
        return cache;
    }

    public void setCache(int cache) {
        this.cache = cache;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public int getDisqueDur() {
        return disqueDur;
    }

    public void setDisqueDur(int disqueDur) {
        this.disqueDur = disqueDur;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getQrcode() {
        return qrcode;
    }

    public void setQrcode(String qrcode) {
        this.qrcode = qrcode;
    }

    public int isDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public String getDateAjout() {
        return dateAjout;
    }

    public void setDateAjout(String dateAjout) {
        this.dateAjout = dateAjout;
    }
}
