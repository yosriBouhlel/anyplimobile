package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.HistoriqueAppareil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour faire le filtrage des historiques de l'appareil selon les dates début et fin
 */

public class SearchHistoriquesAppareilTask extends Thread {

    private Handler mHandler;
    private SearchHistoriquesAppareilListener mSearchHistoriquesAppareilListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;
    private int mHttpStatusCode;
    private ArrayList<HistoriqueAppareil> mHistoriquesAppareil;

    public SearchHistoriquesAppareilTask(Handler mHandler, SearchHistoriquesAppareilListener mSearchHistoriquesAppareilListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mSearchHistoriquesAppareilListener = mSearchHistoriquesAppareilListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();
        mHistoriquesAppareil = new ArrayList<>();

    }

    @Override
    public void run() {
        super.run();

        searchHistoriquesService();

        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();
    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mSearchHistoriquesAppareilListener.onSearchFail(mHistoriquesAppareil);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mSearchHistoriquesAppareilListener.onSearchSuccess(mHistoriquesAppareil);
            }
        });
    }

    public void searchHistoriquesService() {

        String url_server = Outils.API_SERVER + "appareils/historiques/search?appareil_id=" + mParams.get("appareil_id")
                + "&datetimepickerdebut=" + mParams.get("dateDebut")
                + "&datetimepickerfin=" + mParams.get("dateFin");

        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour faire le filtrage des historiques de l'appareil selon les dates début et fin
            mHttpStatusCode = response.code();

            JSONObject historiquesObject = new JSONObject(response.body().string());
            JSONArray historiquesArray = historiquesObject.getJSONArray("result");

            mHistoriquesAppareil.clear();

            for (int i = 0; i < historiquesArray.length(); i++) {
                JSONObject historiqueObject = historiquesArray.getJSONObject(i);
                HistoriqueAppareil historique = new HistoriqueAppareil();

                historique.setHistorique_id(historiqueObject.getInt("historique_id"));
                historique.setEmploye_id(historiqueObject.getInt("employe_id"));
                historique.setNomEmploye(historiqueObject.getString("name"));
                historique.setDateDebut(historiqueObject.getString("debut"));
                historique.setDateFin(historiqueObject.getString("fin"));

                mHistoriquesAppareil.add(historique);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface SearchHistoriquesAppareilListener {

        void onSearchSuccess(ArrayList<HistoriqueAppareil> historiquesAppareil);

        void onSearchFail(ArrayList<HistoriqueAppareil> historiquesAppareil);

    }
}
