package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/*
*
* Thread pour ajouter une nouvelle appareil
*
 */

public class AddAppareilTask extends Thread {

    private Handler mHandler;
    private AddAppareilListener mAddAppareilListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;
    private int mAppareil_id;

    public AddAppareilTask(Handler mHandler, AddAppareilListener mAddAppareilListener, HashMap<String, String> mParams){

        this.mHandler = mHandler;
        this.mAddAppareilListener = mAddAppareilListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (addAppareilService(mParams))
            onSuccess();
        else
            onFail();
    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAddAppareilListener.onFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAddAppareilListener.onSucceed(mAppareil_id);
            }
        });
    }

    public boolean addAppareilService(HashMap<String, String> caractéristiques) {

        String url_server = Outils.API_SERVER + "appareil";

        RequestBody body = new FormBody.Builder()
                .add("categorie", caractéristiques.get("categorie"))
                .add("marque", caractéristiques.get("marque"))
                .add("reference", caractéristiques.get("reference"))
                .add("microprocesseur", caractéristiques.get("microprocesseur"))
                .add("cache", caractéristiques.get("cache"))
                .add("ram", caractéristiques.get("ram"))
                .add("disque_dur", caractéristiques.get("disqueDur"))
                .add("os", caractéristiques.get("os"))
                .add("qrcode", caractéristiques.get("qrcode"))
                .add("disponible", caractéristiques.get("disponible"))
                .build();

        try {

            Response response = ApiCall.POST(mClient, url_server, body); // appel du web service pour l'ajout de l'appareil
            JSONObject appareilObject = new JSONObject(response.body().string());
            mAppareil_id = appareilObject.getInt("id");
            int httpStatusCode = response.code();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;

    }

    public interface AddAppareilListener {

        void onSucceed(int appareil_id);

        void onFail();

    }
}
