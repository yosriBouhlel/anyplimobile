package com.example.bouhlel.anypli.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.global.SessionManager;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/*
*
* Profile de l'utilisateur
*
 */
public class ProfileActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener {


    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.6f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;

    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    private LinearLayout mTitleContainer;
    private TextView mTitle;
    private TextView mName;
    private TextView mUserName;
    private TextView mEmail;
    private CircleImageView mProfilImg;
    private AppBarLayout mAppBarLayout;
    private FloatingActionButton mEditFab;

    private SessionManager mSession;
    private HashMap<String, String> mUser;
    private String mUserImageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        mSession = SessionManager.getInstance(getApplicationContext());
        mUser = mSession.getUserDetails();
        mSession.checkLogin();

        bindActivity();

        // affiche les coordonnées du l'utilisateur
        mName.setText(mUser.get(SessionManager.KEY_NAME));
        mUserName.setText(mUser.get(SessionManager.KEY_NAME));
        mEmail.setText(mUser.get(SessionManager.KEY_EMAIL));
        mTitle.setText(mUser.get(SessionManager.KEY_NAME));

        mUserImageUrl = Outils.SERVER + mUser.get(SessionManager.KEY_IMAGE);

        mAppBarLayout.addOnOffsetChangedListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        startAlphaAnimation(mTitle, 0, View.INVISIBLE);


        mEditFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goUpdateProfil = new Intent(ProfileActivity.this, UpdateProfilActivity.class);
                startActivity(goUpdateProfil);
            }
        });
    }

    private void bindActivity() {
        mTitle = (TextView) findViewById(R.id.main_textview_title);
        mName = (TextView) findViewById(R.id.profil_user_name_id);
        mUserName = (TextView) findViewById(R.id.user_name_id);
        mEmail = (TextView) findViewById(R.id.user_email_id);
        mProfilImg = (CircleImageView) findViewById(R.id.profil_image_id);
        mTitleContainer = (LinearLayout) findViewById(R.id.main_linearlayout_title);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
        mEditFab = (FloatingActionButton) findViewById(R.id.fab);
    }

    // assure l'animation du AppBar
    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }
    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Picasso.with(ProfileActivity.this)
                .load(mUserImageUrl)
                .memoryPolicy(MemoryPolicy.NO_CACHE) // l'image ne sera pas enregistrer dans la cache
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mProfilImg);

    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();

        // calcul du pourcentage de l'apparition du toolbar
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    // assure l'annimation du titre selon le pourcentage de l'apparition du toolbar
    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }


}
