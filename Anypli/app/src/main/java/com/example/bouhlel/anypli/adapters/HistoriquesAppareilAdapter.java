package com.example.bouhlel.anypli.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.holders.HistoriqueAppareilHolder;
import com.example.bouhlel.anypli.holders.ItemClickListener;
import com.example.bouhlel.anypli.models.HistoriqueAppareil;
import com.example.bouhlel.anypli.tasks.DeleteHistoriqueAppareilTask;

import java.util.ArrayList;

/**
 * Created by bouhlel on 21/07/17.
 */

public class HistoriquesAppareilAdapter extends RecyclerView.Adapter<HistoriqueAppareilHolder> implements DeleteHistoriqueAppareilTask.DeleteHistoriqueAppareilListener {

    private Context context;
    private ArrayList<HistoriqueAppareil> historiques;
    private Handler mHandler;


    public HistoriquesAppareilAdapter(Context context, ArrayList<HistoriqueAppareil> historiques) {
        this.context = context;
        this.historiques = historiques;
        mHandler = new Handler();
    }

    @Override
    public HistoriqueAppareilHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appareil_historiques_row, parent, false);
        return new HistoriqueAppareilHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoriqueAppareilHolder holder, int position) {

        final HistoriqueAppareil historique = historiques.get(position);
        holder.bind(historique);

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {

                if (isLongClick) {

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    // Supprimer un élément de l'historique
                                    DeleteHistoriqueAppareilTask deleteHistorique = new DeleteHistoriqueAppareilTask(mHandler, HistoriquesAppareilAdapter.this, String.valueOf(historique.getHistorique_id()), position);
                                    deleteHistorique.start();

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getString(R.string.delete_historique_msg)).setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return historiques.size();
    }

    @Override
    public void onDeleteSuccess(int position) {

        historiques.remove(position);
        notifyItemRemoved(position);
        Toast.makeText(context, context.getResources().getString(R.string.delete_historique_success_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteFail() {

        Toast.makeText(context, context.getResources().getString(R.string.delete_historique_fail_msg), Toast.LENGTH_SHORT).show();
    }


}
