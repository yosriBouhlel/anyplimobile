package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour supprimer une appareil
 */

public class DeleteAppareilTask extends Thread {

    private Handler mHandler;
    private DeleteAppareilListener mDeleteAppareilListener;
    private String mAppareil_id;
    private OkHttpClient mClient;
    private int mPosition;

    public DeleteAppareilTask(Handler mHandler, DeleteAppareilListener mDeleteAppareilListener, String appareil_id, int position){

        this.mHandler = mHandler;
        this.mDeleteAppareilListener = mDeleteAppareilListener;
        this.mAppareil_id = appareil_id;
        this.mPosition = position;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (deleteAppareilService(mAppareil_id))
            onSuccess();
        else
            onFail();
    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteAppareilListener.onDeleteFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteAppareilListener.onDeleteSuccess(mPosition);
            }
        });
    }

    public boolean deleteAppareilService(String appareil_id) {

        String url_server = Outils.API_SERVER + "appareils/delete";

        try {

            RequestBody body = new FormBody.Builder()
                    .add("id", appareil_id)
                    .build();

            Response response = ApiCall.DeleteAppareil(mClient, url_server, body); // appel du web service pour la suppression d'une appareil

            int httpStatusCode = response.code();
            response.body().close();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }

    public interface DeleteAppareilListener {

        void onDeleteSuccess(int position);

        void onDeleteFail();

    }
}
