package com.example.bouhlel.anypli.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Employe;

/**
 * Created by bouhlel on 06/09/17.
 */

public class EmployesHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private TextView mName;
    private TextView mSpecialite;
    private TextView mTel;

    private ItemClickListener mClickListener;

    public EmployesHolder(View itemView) {
        super(itemView);

        mName = (TextView) itemView.findViewById(R.id.employe_name_row_id);
        mSpecialite = (TextView) itemView.findViewById(R.id.employe_specialite_row_id);
        mTel = (TextView) itemView.findViewById(R.id.employe_tel_row_id);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void bind(Employe employe) {

        mName.setText(employe.getNom() + " " + employe.getPrenom());
        mSpecialite.setText(employe.getSpecialite());
        mTel.setText(employe.getTel());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        mClickListener.onClick(v, getPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {

        mClickListener.onClick(v, getPosition(), true);

        return true;
    }
}
