package com.example.bouhlel.anypli.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.global.SessionManager;
import com.example.bouhlel.anypli.tasks.UploadUserImageTask;
import com.example.bouhlel.anypli.ui.fragments.AppareilsFragment;
import com.example.bouhlel.anypli.ui.fragments.DecoderFragment;
import com.example.bouhlel.anypli.ui.fragments.EmployesFragment;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, UploadUserImageTask.UploadUserImageListener {

    private static final int RESULT_LOAD_IMAGE = 1;

    private TextView mNavName;
    private CircleImageView mNavProfilImg;

    private FragmentManager mFragmentManager;
    private SessionManager mSession;
    private HashMap<String, String> mUser;
    private Handler mHandler;

    private File mImageFile;
    private Bitmap mUserImage;
    private String mUserImageUrl;

    private int mFragmentIndex = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mFragmentManager = getSupportFragmentManager();

        // Mettre la liste des appareils comme interface d'acceuil
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new AppareilsFragment())
                .commit();

        // Avoir une instance du session
        mSession = SessionManager.getInstance(getApplicationContext());
        mHandler = new Handler();

        // get user data from session
        mUser = mSession.getUserDetails();
        // Vérifier que l'utilisateur est encore authentifier
        mSession.checkLogin();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.syncState();

        drawer.setDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                // Hide virtual keyboard when opening the drawer menu
                InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(MainActivity.this.getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View hView = navigationView.getHeaderView(0);

        bindActivity(hView);

        // récupérer l'url de l'image du profil
        mUserImageUrl = Outils.SERVER + mUser.get(SessionManager.KEY_IMAGE);

        mNavName.setText(mUser.get(SessionManager.KEY_NAME));


        mNavProfilImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // choisir une image du profil à partir du galerie
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, null), RESULT_LOAD_IMAGE);
            }
        });

        mNavName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
                drawer.closeDrawer(GravityCompat.START);

                Intent goProfile = new Intent(MainActivity.this, ProfileActivity.class);
                startActivity(goProfile);
            }
        });

        navigationView.setNavigationItemSelectedListener(this);


    }

    private void bindActivity(View hView) {

        mNavName = (TextView) hView.findViewById(R.id.nav_header_name_id);
        mNavProfilImg = (CircleImageView) hView.findViewById(R.id.nav_image_id);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            final Uri uri = data.getData();

            try {
                mUserImage = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                persistImage(mUserImage, mUser.get(SessionManager.KEY_ID));

                UploadUserImageTask uploadUserImageTask = new UploadUserImageTask(mHandler, MainActivity.this, mUser.get(SessionManager.KEY_ID), mImageFile);
                uploadUserImageTask.start();

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        mSession.checkLogin();

        switch (mFragmentIndex) {

            case 0: {

                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new AppareilsFragment())
                        .commit();

                getSupportActionBar().setTitle("Appareils");
            }
            break;

            case 1: {

                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new EmployesFragment())
                        .commit();

                getSupportActionBar().setTitle("Employés");
            }
            break;

            case 2: {

                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new DecoderFragment())
                        .commit();

                getSupportActionBar().setTitle("Décoder");
            }
            break;

            default:
                break;
        }

        // Récupérer l'image du profil à partir du l'url
        Picasso.with(MainActivity.this)
                .load(mUserImageUrl)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mNavProfilImg);

    }

    @Override
    public void onBackPressed() {

        // Demande la confirmation de l'utilisateur pour quitter l'application
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {

            drawer.closeDrawer(GravityCompat.START);

        } else {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:

                            MainActivity.super.onBackPressed();

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getResources().getString(R.string.exit_msg)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        // Afficher le fragment adéquat selon le menu item selectionné
        if (id == R.id.nav_equipments) {

            mFragmentIndex = 0;

            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new AppareilsFragment())
                    .commit();

            getSupportActionBar().setTitle("Appareils");

        } else if (id == R.id.nav_employes) {

            mFragmentIndex = 1;

            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new EmployesFragment())
                    .commit();

            getSupportActionBar().setTitle("Employés");

        } else if (id == R.id.nav_decode) {

            mFragmentIndex = 2;

            mFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, new DecoderFragment())
                    .commit();

            getSupportActionBar().setTitle("Décoder");

        } else if (id == R.id.nav_deconnecter) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:
                            mSession.logoutUser();

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setMessage(getResources().getString(R.string.disconnect_msg)).setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    // Compresser l'image bitmap et retourne le File associé
    private void persistImage(Bitmap bitmap, String name) {
        File filesDir = MainActivity.this.getFilesDir();
        mImageFile = new File(filesDir, name + ".png");

        FileOutputStream os;
        try {
            os = new FileOutputStream(mImageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, os);
            os.flush();
            os.close();

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
    }


    // Modifier l'image du profil avec succée
    @Override
    public void onUploadSuccess() {
        Toast.makeText(MainActivity.this, getResources().getString(R.string.update_user_image_success_msg), Toast.LENGTH_LONG).show();

        Picasso.with(MainActivity.this)
                .load(mUserImageUrl)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mNavProfilImg);
    }

    @Override
    public void onUploadFail() {
        Toast.makeText(MainActivity.this, getResources().getString(R.string.update_user_image_fail_msg), Toast.LENGTH_LONG).show();
    }


}
