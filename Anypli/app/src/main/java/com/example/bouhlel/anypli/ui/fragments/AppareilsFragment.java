package com.example.bouhlel.anypli.ui.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.adapters.AppareilsListAdapter;
import com.example.bouhlel.anypli.adapters.CategorieSpinnerAdapter;
import com.example.bouhlel.anypli.models.Appareil;
import com.example.bouhlel.anypli.tasks.AppareilsListTask;
import com.example.bouhlel.anypli.ui.activities.MainActivity;

import java.util.ArrayList;


public class AppareilsFragment extends Fragment implements AppareilsListTask.AppareilsListListener {


    private Spinner mCategorie;
    private RecyclerView mAppareilsList;
    private FloatingActionButton mAddAppareil;
    private SearchView mSearch;

    private ArrayList<Appareil> mAppareils;
    private ArrayList<Appareil> mAppareilsClone;
    private AppareilsListAdapter mAppareilsListAdapter;
    private Handler mHandler;
    private FragmentManager mFragmentManager;


    private Integer[] mCategories = new Integer[]{R.drawable.ic_list_appareil, R.drawable.ic_smartphone, R.drawable.ic_pc, R.drawable.ic_tablet};
    private CategorieSpinnerAdapter mCategorieSpinnerAdapter;


    public AppareilsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_appareils, container, false);

        bindActivity(v);

        mFragmentManager = getFragmentManager();

        mCategorieSpinnerAdapter = new CategorieSpinnerAdapter(mCategories, getContext());
        mCategorie.setAdapter(mCategorieSpinnerAdapter);

        mAppareils = new ArrayList<>();
        mAppareilsClone = new ArrayList<>(); // liste sur laquelle on fait le filtrage des appareils
        mHandler = new Handler();

        // vers l'ajout d'une appareil
        mAddAppareil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new AddAppareilFragment())
                        .commit();
                ((MainActivity) getActivity()).getSupportActionBar().setTitle("Nouveau appareil");
            }
        });


        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            // filtrage selon la catégorie et la marque
            @Override
            public boolean onQueryTextChange(String newText) {

                mAppareils.clear();

                if (mCategorie.getSelectedItemPosition() == 0) {

                    for (int i = 0; i < mAppareilsClone.size(); i++) {
                        if (mAppareilsClone.get(i).getMarque().toLowerCase().contains(newText.toLowerCase()))
                            mAppareils.add(mAppareilsClone.get(i));
                    }

                    mAppareilsListAdapter.notifyDataSetChanged();

                } else {

                    String choix = "Smartphones";

                    switch (mCategorie.getSelectedItemPosition()) {
                        case 1:
                            choix = "Smartphones";
                            break;
                        case 2:
                            choix = "Ordinateurs";
                            break;
                        case 3:
                            choix = "Tablettes";
                            break;
                        default:
                            break;
                    }


                    for (int i = 0; i < mAppareilsClone.size(); i++) {
                        if (mAppareilsClone.get(i).getCategorie().equals(choix) && mAppareilsClone.get(i).getMarque().toLowerCase().contains(newText.toLowerCase()))
                            mAppareils.add(mAppareilsClone.get(i));
                    }

                    mAppareilsListAdapter.notifyDataSetChanged();

                }


                return false;

            }
        });


        // Hide virtual keyboard when touch the recyclerView
        mAppareilsList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSearch.clearFocus();
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        return v;
    }

    private void bindActivity(View v) {
        mCategorie = (Spinner) v.findViewById(R.id.appareils_categorie_id);
        mAppareilsList = (RecyclerView) v.findViewById(R.id.appareils_list_id);
        mAddAppareil = (FloatingActionButton) v.findViewById(R.id.appareils_add_id);
        mSearch = (SearchView) v.findViewById(R.id.appareils_search_id);

        mAppareilsList.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    @Override
    public void onStart() {
        super.onStart();

        mCategorie.setSelection(0);

        mCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String choix = "all";

                mAppareils.clear();

                if (position == 0) {

                    // Récupérer la liste des appareils
                    AppareilsListTask appareilsListTask = new AppareilsListTask(mHandler, AppareilsFragment.this);
                    appareilsListTask.start();

                } else {

                    switch (position) {
                        case 1:
                            choix = "Smartphones";
                            break;
                        case 2:
                            choix = "Ordinateurs";
                            break;
                        case 3:
                            choix = "Tablettes";
                            break;
                        default:
                            break;
                    }


                    for (int i = 0; i < mAppareilsClone.size(); i++) {
                        if (mAppareilsClone.get(i).getCategorie().equals(choix))
                            mAppareils.add(mAppareilsClone.get(i));
                    }

                }

                mAppareilsListAdapter = new AppareilsListAdapter(getContext(), mAppareils);
                mAppareilsList.setAdapter(mAppareilsListAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }


    // cas du succée pour la récupération du liste des appareils
    @Override
    public void onSucceed(ArrayList<Appareil> appareils) {

        this.mAppareils = appareils;
        mAppareilsClone = (ArrayList<Appareil>) appareils.clone();

        mAppareilsListAdapter = new AppareilsListAdapter(getContext(), appareils);
        mAppareilsList.setAdapter(mAppareilsListAdapter);
    }

    @Override
    public void onFail(ArrayList<Appareil> appareils) {

    }


}
