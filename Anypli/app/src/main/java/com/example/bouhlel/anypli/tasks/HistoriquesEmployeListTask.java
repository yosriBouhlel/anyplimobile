package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.HistoriqueEmploye;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour récupérer la liste des historiques de l'employé
 */

public class HistoriquesEmployeListTask extends Thread {

    private Handler mHandler;
    private HistoriquesEmployeListListener mHistoriquesEmployeListListener;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private String employe_id;
    private ArrayList<HistoriqueEmploye> historiqueEmployes;

    public HistoriquesEmployeListTask(Handler mHandler, HistoriquesEmployeListListener mHistoriquesEmployeListListener, String employe_id) {

        this.mHandler = mHandler;
        this.mHistoriquesEmployeListListener = mHistoriquesEmployeListListener;
        this.employe_id = employe_id;
        mClient = new OkHttpClient();
        historiqueEmployes = new ArrayList<>();
    }

    @Override
    public void run() {
        super.run();

        historiquesListService();

        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mHistoriquesEmployeListListener.onFail(historiqueEmployes);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mHistoriquesEmployeListListener.onSucceed(historiqueEmployes);
            }
        });
    }

    public void historiquesListService() {

        String url_server = Outils.API_SERVER + "historique/employes?employe_id=" + employe_id;

        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour récupérer la liste des historiques
            mHttpStatusCode = response.code();

            JSONObject historiquesObject = new JSONObject(response.body().string());
            JSONArray historiquesArray = historiquesObject.getJSONArray("result");

            historiqueEmployes.clear();

            for (int i = 0; i < historiquesArray.length(); i++) {
                JSONObject historiqueObject = historiquesArray.getJSONObject(i);
                HistoriqueEmploye historique = new HistoriqueEmploye();

                historique.setHistorique_id(historiqueObject.getInt("historique_id"));
                historique.setAppareil_id(historiqueObject.getInt("appareil_id"));
                historique.setCategorie(historiqueObject.getString("categorie"));
                historique.setMarque(historiqueObject.getString("marque"));
                historique.setReference(historiqueObject.getString("reference"));
                historique.setDateDebut(historiqueObject.getString("debut"));
                historique.setDateFin(historiqueObject.getString("fin"));

                historiqueEmployes.add(historique);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface HistoriquesEmployeListListener {

        void onSucceed(ArrayList<HistoriqueEmploye> historiqueEmployes);

        void onFail(ArrayList<HistoriqueEmploye> historiqueEmployes);

    }
}
