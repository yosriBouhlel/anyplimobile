package com.example.bouhlel.anypli.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.bouhlel.anypli.R;

/**
 * Created by bouhlel on 28/07/17.
 */

public class CategorieSpinnerAdapter extends BaseAdapter {

    Integer[] categories;
    Context context;


    public CategorieSpinnerAdapter(Integer[] categories, Context context) {

        this.categories = categories;
        this.context = context;
    }

    @Override
    public int getCount() {
        return categories.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = View.inflate(context, R.layout.categories_spinner_row, null);

        ImageView img = (ImageView) convertView.findViewById(R.id.categorie_row_id);


        img.setImageResource(categories[position]);

        return convertView;
    }
}
