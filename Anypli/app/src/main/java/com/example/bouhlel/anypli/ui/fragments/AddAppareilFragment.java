package com.example.bouhlel.anypli.ui.fragments;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.tasks.AddAppareilTask;
import com.example.bouhlel.anypli.tasks.UploadQRImageTask;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.HashMap;

import static android.widget.ArrayAdapter.createFromResource;

public class AddAppareilFragment extends Fragment implements AddAppareilTask.AddAppareilListener, UploadQRImageTask.UploadQRImageListener {

    public final static int QR_CODE_WIDTH = 500;

    private Spinner mCategorie;
    private EditText mMarque;
    private EditText mReference;
    private EditText mMicroprocesseur;
    private EditText mCache;
    private EditText mRam;
    private EditText mDisqueDur;
    private EditText mOS;
    private Button mAjouterBtn;

    private Bitmap mQrCodeBitmap;
    private File mQrCodeImageFile;

    private FragmentManager mFragmentManager;
    private HashMap<String, String> mCaracteristiques;
    private Handler mHandler;
    private ProgressDialog pDialog;


    public AddAppareilFragment() {

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_add_appareil, container, false);

        mFragmentManager = getFragmentManager();
        mCaracteristiques = new HashMap<>(); // caractéristiques de l'appareil

        bindActivity(v);

        ArrayAdapter<CharSequence> adapter = createFromResource(this.getContext(), R.array.categorie_choices, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCategorie.setAdapter(adapter);

        Typeface face = Typeface.createFromAsset(getContext().getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mAjouterBtn.setTypeface(face);

        mAjouterBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (validate()) {

                    pDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog);
                    pDialog.setIndeterminate(true);
                    pDialog.setMessage(getResources().getString(R.string.sign_up_loading_msg));
                    pDialog.show();

                    mCaracteristiques.put("categorie", mCategorie.getSelectedItem().toString());
                    mCaracteristiques.put("marque", mMarque.getText().toString());
                    mCaracteristiques.put("reference", mReference.getText().toString());
                    mCaracteristiques.put("microprocesseur", mMicroprocesseur.getText().toString());
                    mCaracteristiques.put("cache", mCache.getText().toString());
                    mCaracteristiques.put("ram", mRam.getText().toString());
                    mCaracteristiques.put("disqueDur", mDisqueDur.getText().toString());
                    mCaracteristiques.put("os", mOS.getText().toString());
                    mCaracteristiques.put("qrcode", Outils.UPLOADS_SERVER);
                    mCaracteristiques.put("disponible", "1");

                    mHandler = new Handler();

                    // Ajouter une appareil
                    AddAppareilTask addAppareilTask = new AddAppareilTask(mHandler, AddAppareilFragment.this, mCaracteristiques);
                    addAppareilTask.start();

                }
            }
        });


        return v;
    }

    private void bindActivity(View v) {
        mCategorie = (Spinner) v.findViewById(R.id.categorie_id);
        mMarque = (EditText) v.findViewById(R.id.marque_id);
        mReference = (EditText) v.findViewById(R.id.reference_id);
        mMicroprocesseur = (EditText) v.findViewById(R.id.microprocesseur_id);
        mCache = (EditText) v.findViewById(R.id.cache_id);
        mRam = (EditText) v.findViewById(R.id.ram_id);
        mDisqueDur = (EditText) v.findViewById(R.id.disque_dur_id);
        mOS = (EditText) v.findViewById(R.id.os_id);
        mAjouterBtn = (Button) v.findViewById(R.id.ajout_btn_id);
    }

    // vérifier que les infos saisies sont valides
    public boolean validate() {

        boolean valid = true;

        String marque = mMarque.getText().toString();
        String ref = mReference.getText().toString();
        String cache = mCache.getText().toString();
        String ram = mRam.getText().toString();
        String disqueDur = mDisqueDur.getText().toString();

        if (marque.isEmpty() || marque.length() < 3) {
            mMarque.setError(getResources().getString(R.string.sign_up_name_message));
            valid = false;
        } else {
            mMarque.setError(null);
        }

        if (ref.isEmpty()) {
            mReference.setError(getResources().getString(R.string.new_appareil_reference_message));
            valid = false;
        } else {
            mReference.setError(null);
        }

        if (!cache.matches("[0-9]+") && !cache.isEmpty()) {
            mCache.setError(getResources().getString(R.string.new_appareil_entier_message));
            valid = false;
        } else {
            mCache.setError(null);
        }

        if (!ram.matches("[0-9]+") && !ram.isEmpty()) {
            mRam.setError(getResources().getString(R.string.new_appareil_entier_message));
            valid = false;
        } else {
            mRam.setError(null);
        }

        if (!disqueDur.matches("[0-9]+") && !disqueDur.isEmpty()) {
            mDisqueDur.setError(getResources().getString(R.string.new_appareil_entier_message));
            valid = false;
        } else {
            mDisqueDur.setError(null);
        }

        return valid;
    }

    // Compresser l'image bitmap et retourne le File associé
    private void persistImage(Bitmap bitmap, String name) {
        File filesDir = getContext().getFilesDir();
        mQrCodeImageFile = new File(filesDir, name + ".png");

        OutputStream os;
        try {
            os = new FileOutputStream(mQrCodeImageFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
    }

    // Génére le QR code qui contient l'id de l'appareil ajoutée
    private Bitmap TextToImageEncode(String Value) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    Value,
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    QR_CODE_WIDTH, QR_CODE_WIDTH, null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }
        int bitMatrixWidth = bitMatrix.getWidth();

        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {

                pixels[offset + x] = bitMatrix.get(x, y) ?
                        getResources().getColor(R.color.black) : getResources().getColor(R.color.white);
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);

        bitmap.setPixels(pixels, 0, QR_CODE_WIDTH, 0, 0, bitMatrixWidth, bitMatrixHeight);

        return bitmap;
    }

    // cas où l'appareil est ajoutée avec succée
    @Override
    public void onSucceed(int appareil_id) {

        try {
            mQrCodeBitmap = TextToImageEncode(String.valueOf(appareil_id));
        } catch (WriterException e) {
            e.printStackTrace();
        }
        persistImage(mQrCodeBitmap, String.valueOf(appareil_id));

        // upload l'image du QR code au serveur
        UploadQRImageTask uploadImageTask = new UploadQRImageTask(mHandler, AddAppareilFragment.this, String.valueOf(appareil_id), mQrCodeImageFile);
        uploadImageTask.start();
    }

    @Override
    public void onFail() {

        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.new_appareil_fail_msg), Toast.LENGTH_LONG).show();
    }

    // cas où l'upload du QR code est terminé avec succée
    @Override
    public void onUploadSuccess() {

        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.new_appareil_success_msg), Toast.LENGTH_LONG).show();
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new AppareilsFragment())
                .commit();

        mCaracteristiques.clear();
    }

    @Override
    public void onUploadFail() {

        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.new_appareil_fail_msg), Toast.LENGTH_LONG).show();
    }

}
