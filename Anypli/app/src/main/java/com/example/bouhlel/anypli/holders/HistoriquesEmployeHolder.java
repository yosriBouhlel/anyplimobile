package com.example.bouhlel.anypli.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.HistoriqueEmploye;

/**
 * Created by bouhlel on 06/09/17.
 */

public class HistoriquesEmployeHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private TextView mAppareil;
    private TextView mCategorie;
    private TextView mPeriode;

    private ItemClickListener mClickListener;

    public HistoriquesEmployeHolder(View itemView) {
        super(itemView);

        mAppareil = (TextView) itemView.findViewById(R.id.historique_employe_appareil_row_id);
        mCategorie = (TextView) itemView.findViewById(R.id.historique_employe_categorie_row_id);
        mPeriode = (TextView) itemView.findViewById(R.id.historique_employe_periode_row_id);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void bind(HistoriqueEmploye historique) {

        if (!historique.getDateFin().equals("null")) {
            mPeriode.setText(historique.getDateDebut() + " / " + historique.getDateFin());
        } else
            mPeriode.setText(historique.getDateDebut() + " / " + "n\'est pas spécifié");

        mCategorie.setText(historique.getCategorie());
        mAppareil.setText(historique.getMarque() + " " + historique.getReference());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }


    @Override
    public void onClick(View v) {

        mClickListener.onClick(v, getPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {

        mClickListener.onClick(v, getPosition(), true);

        return true;
    }
}
