package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour modifier le profil de l'employé
 */

public class UpdateEmployeProfilTask extends Thread {

    private Handler mHandler;
    private UpdateEmployeProfilListener mUpdateEmployeProfilListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;

    public UpdateEmployeProfilTask(Handler mHandler, UpdateEmployeProfilListener mUpdateEmployeProfilListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mUpdateEmployeProfilListener = mUpdateEmployeProfilListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (updateProfilService())
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateEmployeProfilListener.onUpdateFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateEmployeProfilListener.onUpdateSuccess();
            }
        });
    }

    public boolean updateProfilService() {

        String url_server = Outils.API_SERVER + "employe/update";

        RequestBody body = new FormBody.Builder()
                .add("id", mParams.get("employe_id"))
                .add("nom", mParams.get("nom"))
                .add("prenom", mParams.get("prenom"))
                .add("email", mParams.get("email"))
                .add("specialite", mParams.get("specialite"))
                .add("tel", mParams.get("tel"))
                .build();

        try {

            Response response = ApiCall.updateAppareil(mClient, url_server, body);// appel du web service pour modifier le profil de l'employé
            int httpStatusCode = response.code();
            response.body().close();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }

    public interface UpdateEmployeProfilListener {

        void onUpdateSuccess();

        void onUpdateFail();

    }
}
