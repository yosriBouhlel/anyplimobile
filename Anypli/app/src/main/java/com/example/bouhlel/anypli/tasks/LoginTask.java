package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour assurer l'authentification
 */

public class LoginTask extends Thread {

    private Handler mHandler;
    private LoginListener mLoginListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private String mLoginResponse;
    private User mUser;


    public LoginTask(Handler mHandler, LoginListener mLoginListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mLoginListener = mLoginListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();

    }


    @Override
    public void run() {
        super.run();

        loginService(mParams.get("email"), mParams.get("password"));

        if (mHttpStatusCode == HttpsURLConnection.HTTP_ACCEPTED) {
            onSuccess();
        } else {
            onFail();
        }

    }


    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mLoginListener.onFail(mLoginResponse);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mLoginListener.onSucceed(mUser, mLoginResponse);
            }
        });
    }


    private void loginService(String email, String password) {

        String url_server = Outils.API_SERVER + "user/login?email=" + email + "&password=" + password;

        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour assurer l'authentification
            String responseData = response.body().string();
            JSONObject json = new JSONObject(responseData);
            mHttpStatusCode = response.code();

            mUser = new User();
            mLoginResponse = json.getString("result");

            if (mHttpStatusCode == HttpsURLConnection.HTTP_ACCEPTED) {
                mUser.setId(json.getString("user_id"));
                mUser.setName(json.getString("name"));
                mUser.setEmail(json.getString("email"));
                mUser.setPassword(json.getString("password"));
                mUser.setImage(json.getString("image"));
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface LoginListener {

        void onSucceed(User user, String loginResponse);

        void onFail(String loginResponse);
    }
}
