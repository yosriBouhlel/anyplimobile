package com.example.bouhlel.anypli.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Appareil;

/**
 * Created by bouhlel on 07/09/17.
 */

public class AppareilsHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private TextView mDisponible;
    private TextView mCategorie;
    private TextView mMarque;

    private ItemClickListener mClickListener;

    public AppareilsHolder(View itemView) {
        super(itemView);

        mDisponible = (TextView) itemView.findViewById(R.id.appareils_disponible_row_id);
        mCategorie = (TextView) itemView.findViewById(R.id.appareils_categorie_row_id);
        mMarque = (TextView) itemView.findViewById(R.id.appareils_marque_row_id);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void bind(Appareil appareil) {

        switch (appareil.isDisponible()) {
            case 1:
                mDisponible.setText("OUI");
                break;
            case 0:
                mDisponible.setText("NON");
                break;
            default:
                break;
        }

        mCategorie.setText(appareil.getCategorie());
        mMarque.setText(appareil.getMarque());
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        mClickListener.onClick(v, getPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {

        mClickListener.onClick(v, getPosition(), true);

        return true;
    }
}
