package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.File;
import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour l'upload de l'image du profil de l'utilisateur au serveur
 */

public class UploadUserImageTask extends Thread {

    private Handler mHandler;
    private UploadUserImageListener mUploadUserImageListener;
    private String mTitle;
    private OkHttpClient mClient;

    private File mImageFile;

    public UploadUserImageTask(Handler mHandler, UploadUserImageListener mUploadUserImageListener, String title, File imageFile) {

        this.mImageFile = imageFile;
        this.mHandler = mHandler;
        this.mUploadUserImageListener = mUploadUserImageListener;
        this.mTitle = title;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (uploadUserImageService(mTitle))
            onSuccess();
        else
            onFail();


    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUploadUserImageListener.onUploadFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUploadUserImageListener.onUploadSuccess();
            }
        });
    }

    public boolean uploadUserImageService(String title) {

        String url_server = Outils.API_SERVER + "user/upload";

        try {

            Response response = ApiCall.POST(mClient, url_server, ApiCall.uploadRequestBody(title, "png", mImageFile));// appel du web service pour l'upload de l'image du profil de l'utilisateur au serveur
            int httpStatusCode = response.code();
            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface UploadUserImageListener {

        void onUploadSuccess();

        void onUploadFail();

    }
}
