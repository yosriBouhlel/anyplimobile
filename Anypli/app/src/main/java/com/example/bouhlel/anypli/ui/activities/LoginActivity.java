package com.example.bouhlel.anypli.ui.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.global.SessionManager;
import com.example.bouhlel.anypli.models.User;
import com.example.bouhlel.anypli.tasks.LoginTask;

import java.util.HashMap;

/*
*
* Assure l'authentification de l'utilisateur pour accéder à l'application
*
 */
public class LoginActivity extends AppCompatActivity implements LoginTask.LoginListener {

    private EditText mEmail;
    private EditText mPassword;
    private Button mConnectionBtn;

    private SessionManager mSession;
    private Handler mHandler;

    private ProgressDialog pDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mSession = SessionManager.getInstance(getApplicationContext());
        mHandler = new Handler();

        bindActivity();

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mConnectionBtn.setTypeface(face);

        mConnectionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {

                    mConnectionBtn.setEnabled(false);

                    pDialog = new ProgressDialog(LoginActivity.this, R.style.Theme_AppCompat_DayNight_Dialog);
                    pDialog.setIndeterminate(true);
                    pDialog.setMessage(getResources().getString(R.string.login_loading_msg));
                    pDialog.show();

                    HashMap<String, String> params = new HashMap<>();
                    params.put("email", mEmail.getText().toString());
                    params.put("password", mPassword.getText().toString());

                    LoginTask login = new LoginTask(mHandler, LoginActivity.this, params);
                    login.start();

                }

            }
        });

    }

    private void bindActivity(){

        mEmail = (EditText) findViewById(R.id.email_field);
        mPassword = (EditText) findViewById(R.id.password_field);
        mConnectionBtn = (Button) findViewById(R.id.connection_button);
    }

    // Hide virtual keyboard when touch out side edittext
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }


    // remplir la session par les infos de l'utilisateur et le redirige vers l'acceuil
    @Override
    public void onSucceed(User user, String loginResponse) {

        mSession.createLoginSession(user.getName(), user.getEmail(), user.getPassword(), user.getId(), loginResponse, user.getImage());
        Intent goHome = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(goHome);
        finish();

        pDialog.dismiss();
    }

    @Override
    public void onFail(String loginResponse) {

        pDialog.dismiss();
        mConnectionBtn.setEnabled(true);
        Toast.makeText(LoginActivity.this, loginResponse, Toast.LENGTH_LONG).show();
    }


    // vérifier que l'email et le password saisies sont valides
    public boolean validate() {
        boolean valid = true;

        String email = mEmail.getText().toString();
        String password = mPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError(getResources().getString(R.string.sign_up_email_message));
            valid = false;
        } else {
            mEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 8) {
            mPassword.setError(getResources().getString(R.string.sign_up_password_message));
            valid = false;
        } else {
            mPassword.setError(null);
        }

        return valid;
    }


}
