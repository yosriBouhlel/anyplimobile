package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour supprimer un élément de l'historique d'un employé
 */

public class DeleteHistoriqueEmployeTask extends Thread {

    private Handler mHandler;
    private DeleteHistoriqueEmployeListener mDeleteHistoriqueEmployeListener;
    private String mHistorique_id;
    private OkHttpClient mClient;
    private int mPosition;

    public DeleteHistoriqueEmployeTask(Handler mHandler, DeleteHistoriqueEmployeListener mDeleteHistoriqueEmployeListener, String historique_id, int position) {

        this.mHandler = mHandler;
        this.mDeleteHistoriqueEmployeListener = mDeleteHistoriqueEmployeListener;
        this.mHistorique_id = historique_id;
        this.mPosition = position;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (deleteHistoriqueService())
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteHistoriqueEmployeListener.onDeleteFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteHistoriqueEmployeListener.onDeleteSuccess(mPosition);
            }
        });
    }

    public boolean deleteHistoriqueService() {

        String url_server = Outils.API_SERVER + "historiques/delete";

        try {

            RequestBody body = new FormBody.Builder()
                    .add("id", mHistorique_id)
                    .build();

            Response response = ApiCall.DeleteAppareil(mClient, url_server, body);// appel du web service pour la suppression d'un élément de l'historique d'un employé

            int httpStatusCode = response.code();

            response.body().close();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface DeleteHistoriqueEmployeListener {

        void onDeleteSuccess(int position);

        void onDeleteFail();

    }
}
