package com.example.bouhlel.anypli.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.holders.EmployesHolder;
import com.example.bouhlel.anypli.holders.ItemClickListener;
import com.example.bouhlel.anypli.models.Employe;
import com.example.bouhlel.anypli.tasks.DeleteEmployeTask;
import com.example.bouhlel.anypli.ui.activities.EmployeDetailsActivity;

import java.util.ArrayList;

/**
 * Created by bouhlel on 26/07/17.
 */

public class EmployesListAdapter extends RecyclerView.Adapter<EmployesHolder> implements DeleteEmployeTask.DeleteEmployeListener {

    private Context context;
    private ArrayList<Employe> employes;
    private Handler mHandler;


    public EmployesListAdapter(Context context, ArrayList<Employe> employes) {
        this.context = context;
        this.employes = employes;
        mHandler = new Handler();
    }

    @Override
    public EmployesHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employes_list_row, parent, false);

        return new EmployesHolder(view);
    }

    @Override
    public void onBindViewHolder(EmployesHolder holder, int position) {

        final Employe employe = employes.get(position);

        holder.bind(employe);

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {

                if (isLongClick) {
                    // Demander la confirmation pour supprimer un employé
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    // supprimer un employé
                                    DeleteEmployeTask deleteEmployeTask = new DeleteEmployeTask(mHandler, EmployesListAdapter.this, String.valueOf(employe.getEmploye_id()), position);
                                    deleteEmployeTask.start();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getResources().getString(R.string.delete_employe_msg)).setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                } else {

                    // vers le profil de l'employé
                    Intent goDetailsEmploye = new Intent(context, EmployeDetailsActivity.class);
                    goDetailsEmploye.putExtra("employe", employe);
                    context.startActivity(goDetailsEmploye);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return employes.size();
    }

    // cas de succée pour la suppression d'un employé
    @Override
    public void onDeleteSuccess(int position) {

        employes.remove(position);
        notifyItemRemoved(position);
        Toast.makeText(context, context.getResources().getString(R.string.delete_employe_success_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteFail() {

        Toast.makeText(context, context.getResources().getString(R.string.delete_employe_fail_msg), Toast.LENGTH_SHORT).show();
    }


}
