package com.example.bouhlel.anypli.ui.fragments;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.tasks.AddEmployeTask;

import java.util.HashMap;


public class AddEmployeFragment extends Fragment implements AddEmployeTask.AddEmployeListener {

    private EditText mFirstName;
    private EditText mLastName;
    private EditText mSpecialite;
    private EditText mEmail;
    private EditText mTel;
    private Button mAddBtn;

    private HashMap<String, String> mCoordonnees;

    private FragmentManager mFragmentManager;
    private Handler mHandler;
    private ProgressDialog pDialog;


    public AddEmployeFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_add_employe, container, false);

        mFragmentManager = getFragmentManager();
        mCoordonnees = new HashMap<>(); // coordonnées de l'employé
        mHandler = new Handler();

        bindActivity(v);

        Typeface face = Typeface.createFromAsset(getContext().getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mAddBtn.setTypeface(face);

        mAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {

                    pDialog = new ProgressDialog(getContext(), R.style.Theme_AppCompat_DayNight_Dialog);
                    pDialog.setIndeterminate(true);
                    pDialog.setMessage(getResources().getString(R.string.sign_up_loading_msg));
                    pDialog.show();

                    mCoordonnees.put("firstName", mFirstName.getText().toString());
                    mCoordonnees.put("LastName", mLastName.getText().toString());
                    mCoordonnees.put("specialite", mSpecialite.getText().toString());
                    mCoordonnees.put("email", mEmail.getText().toString());
                    mCoordonnees.put("tel", mTel.getText().toString());

                    // Ajouter un employé
                    AddEmployeTask addEmployeTask = new AddEmployeTask(mHandler, AddEmployeFragment.this, mCoordonnees);
                    addEmployeTask.start();

                }
            }
        });


        return v;
    }

    private void bindActivity(View v) {
        mFirstName = (EditText) v.findViewById(R.id.add_employe_firstname_id);
        mLastName = (EditText) v.findViewById(R.id.add_employe_lastname_id);
        mSpecialite = (EditText) v.findViewById(R.id.add_employe_specialite_id);
        mEmail = (EditText) v.findViewById(R.id.add_employe_email_id);
        mTel = (EditText) v.findViewById(R.id.add_employe_tel_id);
        mAddBtn = (Button) v.findViewById(R.id.add_employe_button_id);
    }

    // vérifier que les infos saisies sont valides
    public boolean validate() {

        boolean valid = true;

        String firstName = mFirstName.getText().toString();
        String lastName = mLastName.getText().toString();
        String email = mEmail.getText().toString();
        String tel = mTel.getText().toString();

        if (firstName.isEmpty() || firstName.length() < 3) {
            mFirstName.setError(getResources().getString(R.string.sign_up_name_message));
            valid = false;
        } else {
            mFirstName.setError(null);
        }

        if (lastName.isEmpty() || lastName.length() < 3) {
            mLastName.setError(getResources().getString(R.string.sign_up_name_message));
            valid = false;
        } else {
            mLastName.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError(getResources().getString(R.string.sign_up_email_message));
            valid = false;
        } else {
            mEmail.setError(null);
        }

        if (!Patterns.PHONE.matcher(tel).matches() || mTel.length() < 8) {
            mTel.setError(getResources().getString(R.string.phone_number_message));
            valid = false;
        } else {
            mTel.setError(null);
        }

        return valid;
    }

    // cas où l'employé est ajouté avec succée
    @Override
    public void onSucceed() {

        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.new_employe_success_msg), Toast.LENGTH_LONG).show();
        mFragmentManager.beginTransaction()
                .replace(R.id.fragment_container, new EmployesFragment())
                .commit();

        mCoordonnees.clear();

    }

    @Override
    public void onFail() {

        pDialog.dismiss();
        Toast.makeText(getContext(), getResources().getString(R.string.new_employe_fail_msg), Toast.LENGTH_LONG).show();
    }


}
