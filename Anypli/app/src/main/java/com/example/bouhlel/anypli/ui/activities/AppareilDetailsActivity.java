package com.example.bouhlel.anypli.ui.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Appareil;
import com.example.bouhlel.anypli.tasks.AppareilDetailsTask;
import com.example.bouhlel.anypli.tasks.UpdateAppareilTask;
import com.squareup.picasso.Picasso;


/*
*
* Afficher les caractéristiques d'une appareil
*
 */
public class AppareilDetailsActivity extends AppCompatActivity implements AppareilDetailsTask.AppareilDetailsListener, UpdateAppareilTask.UpdateAppareilListener {

    private static final int REQUEST_CODE = 1;

    com.github.clans.fab.FloatingActionButton mAffecterFab;
    private TextView mDisponible;
    private TextView mAffecterA;
    private TextView mCategorie;
    private TextView mMarque;
    private TextView mReference;
    private TextView mMicroprocesseur;
    private TextView mCache;
    private TextView mRam;
    private TextView mDisqueDur;
    private TextView mOS;
    private ImageView mQrcode;
    private LinearLayout mAffecterLayout;

    private Appareil appareil;
    private int appareil_id;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appareil_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout)
                findViewById(R.id.toolbar_layout);

        toolbarLayout.setTitle(getResources().getString(R.string.caracteristiques_label));

        Bundle bundle = getIntent().getExtras();
        appareil_id = bundle.getInt("appareil_id");

        bindActivity();

        mAffecterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch (appareil.isDisponible()) {

                    case 0: {

                        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                switch (which) {
                                    case DialogInterface.BUTTON_POSITIVE:

                                        // Libérer l'appareil de l'employé
                                        UpdateAppareilTask updateAppareilTask = new UpdateAppareilTask(mHandler, AppareilDetailsActivity.this, appareil.getAppareil_id());
                                        updateAppareilTask.start();

                                        break;

                                    case DialogInterface.BUTTON_NEGATIVE:
                                        break;
                                }
                            }
                        };

                        AlertDialog.Builder builder = new AlertDialog.Builder(AppareilDetailsActivity.this);
                        builder.setMessage(getResources().getString(R.string.update_appareil_msg)).setPositiveButton("Yes", dialogClickListener)
                                .setNegativeButton("No", dialogClickListener).show();

                    }
                    break;

                    case 1: {

                        // Aller à l'interface de l'affectation de l'appareil
                        Intent affecterActivity = new Intent(AppareilDetailsActivity.this, AffecterActivity.class);
                        affecterActivity.putExtra("appareil_id", appareil_id);
                        // Pour vérifier que l'appareil a été affecter ou l'opération a été annuler
                        startActivityForResult(affecterActivity, REQUEST_CODE);

                    }
                    break;
                    default:
                        break;
                }


            }
        });


        com.github.clans.fab.FloatingActionButton avanceFab = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.appareil_avance_fab);
        avanceFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Consulter l'historique de l'appareil
                Intent goHistoriqueAppareil = new Intent(AppareilDetailsActivity.this, HistoriqueAppareilActivity.class);
                goHistoriqueAppareil.putExtra("appareil_id", appareil.getAppareil_id());
                startActivity(goHistoriqueAppareil);
            }
        });

    }


    private void bindActivity() {

        mDisponible = (TextView) findViewById(R.id.details_disponible_id);
        mAffecterA = (TextView) findViewById(R.id.details_affecter_id);
        mCategorie = (TextView) findViewById(R.id.details_categorie_id);
        mMarque = (TextView) findViewById(R.id.details_marque_id);
        mReference = (TextView) findViewById(R.id.details_reference_id);
        mMicroprocesseur = (TextView) findViewById(R.id.details_microprocesseur_id);
        mCache = (TextView) findViewById(R.id.details_cache_id);
        mRam = (TextView) findViewById(R.id.details_ram_id);
        mDisqueDur = (TextView) findViewById(R.id.details_disque_dur_id);
        mOS = (TextView) findViewById(R.id.details_os_id);
        mQrcode = (ImageView) findViewById(R.id.details_qrcode_id);
        mAffecterLayout = (LinearLayout) findViewById(R.id.affecter_layout_id);
        mAffecterFab = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.appareil_affecter_fab);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {

            case RESULT_OK: {

                Bundle resultData = data.getExtras();
                String employeName = resultData.getString("employeName");

                mAffecterA.setText(employeName);
                appareil.setDisponible(0);
                mAffecterLayout.setVisibility(View.VISIBLE);
                mDisponible.setText("NON");
                mAffecterFab.setLabelText("Libérer");

                break;
            }

            case RESULT_CANCELED:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();

        mHandler = new Handler();

        // Récupérer les caractéristiques de l'appareil
        AppareilDetailsTask appareilDetailsTask = new AppareilDetailsTask(mHandler, AppareilDetailsActivity.this, AppareilDetailsActivity.this, appareil_id);
        appareilDetailsTask.start();

    }


    // Récupération des caractéristiques avec succées
    @Override
    public void onSucceed(Appareil appareil, String employeName) {

        this.appareil = appareil;

        switch (appareil.isDisponible()) {

            case 0: {
                mAffecterLayout.setVisibility(View.VISIBLE);
                mDisponible.setText(getResources().getString(R.string.appareil_non_disponible_msg));
                mAffecterFab.setLabelText(getResources().getString(R.string.appareil_libre_msg));
                mAffecterFab.setImageResource(R.drawable.ic_liberer);
            }
            break;

            case 1: {
                mAffecterLayout.setVisibility(View.GONE);
                mDisponible.setText(getResources().getString(R.string.appareil_disponible_msg));
                mAffecterFab.setLabelText(getResources().getString(R.string.appareil_affecter_msg));
                mAffecterFab.setImageResource(R.drawable.ic_affecter);
            }
            break;

            default:
                break;
        }

        mAffecterA.setText(employeName);
        mCategorie.setText(appareil.getCategorie());
        mMarque.setText(appareil.getMarque());
        mReference.setText(appareil.getReference());
        mMicroprocesseur.setText(appareil.getMicroprocesseur());
        mCache.setText(String.valueOf(appareil.getCache()));
        mRam.setText(String.valueOf(appareil.getRam()));
        mDisqueDur.setText(String.valueOf(appareil.getDisqueDur()));
        mOS.setText(appareil.getOs());

        Picasso.with(AppareilDetailsActivity.this)
                .load(Outils.SERVER + appareil.getQrcode())
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mQrcode);
    }

    @Override
    public void onFail() {

        Toast.makeText(AppareilDetailsActivity.this, getResources().getString(R.string.appareil_not_found_msg), Toast.LENGTH_SHORT).show();
        finish();
    }


    // Succées de la libération de l'appareil
    @Override
    public void onUpdateSuccess() {
        mAffecterLayout.setVisibility(View.GONE);
        mAffecterFab.setLabelText(getResources().getString(R.string.appareil_affecter_msg));
        mAffecterFab.setImageResource(R.drawable.ic_affecter);
        mDisponible.setText(getResources().getString(R.string.appareil_disponible_msg));
        appareil.setDisponible(1);
        Toast.makeText(AppareilDetailsActivity.this, getResources().getString(R.string.update_appareil_success_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUpdateFail() {
        Toast.makeText(AppareilDetailsActivity.this, getResources().getString(R.string.update_appareil_fail_msg), Toast.LENGTH_SHORT).show();
    }


}
