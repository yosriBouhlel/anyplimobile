package com.example.bouhlel.anypli.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Employe;

/*
*
* Afficher les caractéristiques d'un employé
*
**/

public class EmployeDetailsActivity extends AppCompatActivity {

    private static final int REQUEST_CODE = 1;

    CollapsingToolbarLayout toolbarLayout;
    private TextView mName;
    private TextView mSpecialite;
    private TextView mEmail;
    private TextView mTel;
    private Employe employe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employe_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbarLayout = (CollapsingToolbarLayout)
                findViewById(R.id.toolbar_layout);

        Bundle bundle = getIntent().getExtras();
        employe = (Employe) bundle.getSerializable("employe");

        bindActivity();

        com.github.clans.fab.FloatingActionButton avanceFab = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.employe_avance_fab);
        avanceFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Consulter l'historique de l'employé
                Intent goHistoriqueEmploye = new Intent(EmployeDetailsActivity.this, HistoriqueEmployeActivity.class);
                goHistoriqueEmploye.putExtra("employe_id", employe.getEmploye_id());
                startActivity(goHistoriqueEmploye);
            }
        });


        com.github.clans.fab.FloatingActionButton updateFab = (com.github.clans.fab.FloatingActionButton) findViewById(R.id.employe_update_fab);
        updateFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Modifier le profil de l'employé
                Intent updateProfil = new Intent(EmployeDetailsActivity.this, UpdateEmployeProfilActivity.class);
                updateProfil.putExtra("employe", employe);

                // Retourne les coordonnées de l'employé aprés modification
                startActivityForResult(updateProfil, REQUEST_CODE);
            }
        });

        toolbarLayout.setTitle(employe.getNom() + " " + employe.getPrenom());
        mName.setText(employe.getNom() + " " + employe.getPrenom());
        mEmail.setText(employe.getEmail());
        mSpecialite.setText(employe.getSpecialite());
        mTel.setText(employe.getTel());

    }

    private void bindActivity() {
        mName = (TextView) findViewById(R.id.details_employe_name_id);
        mSpecialite = (TextView) findViewById(R.id.details_employe_specialite_id);
        mEmail = (TextView) findViewById(R.id.details_employe_email_id);
        mTel = (TextView) findViewById(R.id.details_employe_tel_id);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {

            case RESULT_OK: {

                Bundle resultData = data.getExtras();
                employe = (Employe) resultData.getSerializable("employe");

                toolbarLayout.setTitle(employe.getNom() + " " + employe.getPrenom());
                mName.setText(employe.getNom() + " " + employe.getPrenom());
                mEmail.setText(employe.getEmail());
                mSpecialite.setText(employe.getSpecialite());
                mTel.setText(employe.getTel());

                break;
            }

            case RESULT_CANCELED:
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
}
