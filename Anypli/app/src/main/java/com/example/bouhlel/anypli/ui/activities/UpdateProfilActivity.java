package com.example.bouhlel.anypli.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.global.SessionManager;
import com.example.bouhlel.anypli.tasks.UpdateUserProfilTask;
import com.example.bouhlel.anypli.tasks.UploadUserImageTask;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

/*
*
* Mettre à jour le profil de l'utilisateur
*
 */
public class UpdateProfilActivity extends AppCompatActivity implements AppBarLayout.OnOffsetChangedListener, UpdateUserProfilTask.UpdateUserProfilListener, UploadUserImageTask.UploadUserImageListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.6f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    private static final int RESULT_LOAD_IMAGE = 1;

    private boolean mIsTheTitleVisible = false;
    private boolean mIsTheTitleContainerVisible = true;

    private LinearLayout mTitleContainer;
    private TextView mTitle;
    private TextView mName;
    private EditText mUserName;
    private EditText mEmail;
    private EditText mAncienPassword;
    private EditText mNewPassword;
    private EditText mConfirmPassword;
    private Button mModifier;
    private TextView mPasswordLink;
    private CircleImageView mProfilImg;
    private LinearLayout mUpdatePasswordBlock;
    private AppBarLayout mAppBarLayout;

    private SessionManager mSession;
    private HashMap<String, String> mUser;
    private Handler mHandler;
    private File mImageFile;
    private Bitmap mUserImage;
    private HashMap<String, String> mParams;

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mSession = SessionManager.getInstance(getApplicationContext());
        mUser = mSession.getUserDetails();
        mSession.checkLogin();

        bindActivity();

        mName.setText(mUser.get(SessionManager.KEY_NAME));
        mUserName.setText(mUser.get(SessionManager.KEY_NAME));
        mEmail.setText(mUser.get(SessionManager.KEY_EMAIL));
        mTitle.setText(mUser.get(SessionManager.KEY_NAME));

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mModifier.setTypeface(face);

        mHandler = new Handler();
        mParams = new HashMap<>();

        final String userImageUrl = Outils.SERVER + mUser.get(SessionManager.KEY_IMAGE);

        // Récupérer l'image du profil à partir du l'url
        Picasso.with(UpdateProfilActivity.this)
                .load(userImageUrl)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .placeholder(R.drawable.ic_user)
                .error(R.drawable.ic_user)
                .into(mProfilImg);


        // affiche le block pour la modification du mot de passe
        mPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mUpdatePasswordBlock.setVisibility(View.VISIBLE);
                mPasswordLink.setVisibility(View.GONE);
            }
        });

        mModifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (validate()) {

                    mParams.put("name", mUserName.getText().toString());
                    mParams.put("email", mEmail.getText().toString());
                    mParams.put("ancienpassword", mAncienPassword.getText().toString());
                    mParams.put("newpassword", mNewPassword.getText().toString());
                    mParams.put("token", mUser.get(SessionManager.KEY_TOKEN));

                    // Mettre à jour le profil de l'utilisateur
                    UpdateUserProfilTask updateProfilTask = new UpdateUserProfilTask(mHandler, UpdateProfilActivity.this, mParams);
                    updateProfilTask.start();

                }
            }
        });

        mProfilImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // choisir une image du profil à partir du galerie
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);

                startActivityForResult(Intent.createChooser(intent, "Select Image From Gallery"), RESULT_LOAD_IMAGE);
            }
        });

        mAppBarLayout.addOnOffsetChangedListener(this);


        startAlphaAnimation(mTitle, 0, View.INVISIBLE);
    }

    private void bindActivity() {
        mTitle = (TextView) findViewById(R.id.main_textview_title);
        mName = (TextView) findViewById(R.id.title_user_name_id);
        mUserName = (EditText) findViewById(R.id.profil_name_id);
        mEmail = (EditText) findViewById(R.id.profil_email_id);
        mPasswordLink = (TextView) findViewById(R.id.update_user_password_link);
        mAncienPassword = (EditText) findViewById(R.id.profil_password_id);
        mNewPassword = (EditText) findViewById(R.id.profil_new_password_id);
        mConfirmPassword = (EditText) findViewById(R.id.profil_confirm_password_id);
        mUpdatePasswordBlock = (LinearLayout) findViewById(R.id.update_password_block_id);
        mModifier = (Button) findViewById(R.id.modifier_btn_id);
        mProfilImg = (CircleImageView) findViewById(R.id.update_image_id);
        mTitleContainer = (LinearLayout) findViewById(R.id.main_linearlayout_title);
        mAppBarLayout = (AppBarLayout) findViewById(R.id.main_appbar);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            final Uri uri = data.getData();

            try {
                mUserImage = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                persistImage(mUserImage, mUser.get(SessionManager.KEY_ID));

                // upload l'image du profil de l'utilisateur au serveur
                UploadUserImageTask uploadUserImageTask = new UploadUserImageTask(mHandler, UpdateProfilActivity.this, mUser.get(SessionManager.KEY_ID), mImageFile);
                uploadUserImageTask.start();

            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    // Hide virtual keyboard when touch out side EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleVisible = true;
            }

        } else {

            if (mIsTheTitleVisible) {
                startAlphaAnimation(mTitle, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleVisible = false;
            }
        }
    }

    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                mIsTheTitleContainerVisible = false;
            }

        } else {

            if (!mIsTheTitleContainerVisible) {
                startAlphaAnimation(mTitleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                mIsTheTitleContainerVisible = true;
            }
        }
    }

    // vérifier que les infos saisies sont valides
    public boolean validate() {
        boolean valid = true;

        String email = mEmail.getText().toString();
        String ancienPassword = mAncienPassword.getText().toString();
        String newPassword = mNewPassword.getText().toString();
        String confirmPassword = mConfirmPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError(getResources().getString(R.string.sign_up_email_message));
            valid = false;
        } else {
            mEmail.setError(null);
        }

        if (mUpdatePasswordBlock.getVisibility() == View.VISIBLE) {

            if (ancienPassword.isEmpty()) {
                mAncienPassword.setError(getResources().getString(R.string.profil_password_message));
                valid = false;
            } else {
                mAncienPassword.setError(null);
            }

            if (newPassword.isEmpty() || newPassword.length() < 8) {
                mNewPassword.setError(getResources().getString(R.string
                        .sign_up_password_message));
                valid = false;
            } else {
                mNewPassword.setError(null);
            }

            if (confirmPassword.isEmpty() || !mConfirmPassword.getText().toString().equals(mNewPassword.getText().toString())) {
                mConfirmPassword.setError(getResources().getString(R.string.sign_up_passwordConfirmation_message));
                valid = false;
            } else {
                mConfirmPassword.setError(null);
            }

        }

        return valid;
    }

    // Compresser l'image bitmap et retourne le File associé
    private void persistImage(Bitmap bitmap, String name) {
        File filesDir = UpdateProfilActivity.this.getFilesDir();
        mImageFile = new File(filesDir, name + ".png");

        FileOutputStream os;
        try {
            os = new FileOutputStream(mImageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, os);
            os.flush();
            os.close();

        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
    }

    @Override
    public void onUpdateSuccess() {
        Toast.makeText(UpdateProfilActivity.this, getResources().getString(R.string.update_user_success_msg), Toast.LENGTH_LONG).show();

        mSession.logoutUser();
    }

    @Override
    public void onUpdateFail() {
        Toast.makeText(UpdateProfilActivity.this, getResources().getString(R.string.update_user_fail_msg), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onUploadSuccess() {
        Toast.makeText(UpdateProfilActivity.this, getResources().getString(R.string.update_user_image_success_msg), Toast.LENGTH_LONG).show();

        mProfilImg.setImageBitmap(mUserImage);
    }

    @Override
    public void onUploadFail() {
        Toast.makeText(UpdateProfilActivity.this, getResources().getString(R.string.update_user_image_fail_msg), Toast.LENGTH_LONG).show();
    }


}
