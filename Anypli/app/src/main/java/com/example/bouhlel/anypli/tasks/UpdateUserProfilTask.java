package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour modifier le profil de l'utilisateur
 */

public class UpdateUserProfilTask extends Thread {

    private Handler mHandler;
    private UpdateUserProfilListener mUpdateUserProfilListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;

    public UpdateUserProfilTask(Handler mHandler, UpdateUserProfilListener mUpdateUserProfilListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mUpdateUserProfilListener = mUpdateUserProfilListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (updateProfilService())
            onSuccess();
        else
            onFail();

    }


    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateUserProfilListener.onUpdateFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateUserProfilListener.onUpdateSuccess();
            }
        });
    }


    public boolean updateProfilService() {

        String url_server = Outils.API_SERVER + "user/update";

        RequestBody body = new FormBody.Builder()
                .add("name", mParams.get("name"))
                .add("email", mParams.get("email"))
                .add("ancienpassword", mParams.get("ancienpassword"))
                .add("newpassword", mParams.get("newpassword"))
                .build();

        try {

            Response response = ApiCall.PUT(mClient, url_server, body, mParams.get("token")); // appel du web service pour modifier le profil de l'utilisateur
            int httpStatusCode = response.code();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


    public interface UpdateUserProfilListener {

        void onUpdateSuccess();

        void onUpdateFail();

    }
}
