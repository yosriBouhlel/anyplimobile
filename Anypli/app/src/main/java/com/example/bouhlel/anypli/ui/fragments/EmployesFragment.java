package com.example.bouhlel.anypli.ui.fragments;


import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.adapters.EmployesListAdapter;
import com.example.bouhlel.anypli.models.Employe;
import com.example.bouhlel.anypli.tasks.EmployesListTask;
import com.example.bouhlel.anypli.ui.activities.MainActivity;

import java.util.ArrayList;


public class EmployesFragment extends Fragment implements EmployesListTask.EmployesListListener {


    private RecyclerView mEmployesList;
    private SearchView mSearch;
    private FloatingActionButton mAddEmploye;

    private ArrayList<Employe> mEmployes;
    private ArrayList<Employe> mEmployesClone; // liste avec laquelle on fait le filtrage des employés
    private EmployesListAdapter mEmployesListAdapter;

    private Handler mHandler;
    private FragmentManager mFragmentManager;

    public EmployesFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_employes, container, false);

        bindActivity(v);

        mFragmentManager = getFragmentManager();
        mEmployes = new ArrayList<>();
        mHandler = new Handler();

        // vers l'ajout d'un employé
        mAddEmploye.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container, new AddEmployeFragment())
                        .commit();
                ((MainActivity) getActivity()).getSupportActionBar().setTitle("Nouveau employé");
            }
        });

        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            // filtrage selon le nom et le prénom de l'employé
            @Override
            public boolean onQueryTextChange(String newText) {

                mEmployes.clear();

                for (int i = 0; i < mEmployesClone.size(); i++) {
                    if ((mEmployesClone.get(i).getNom().toLowerCase() + " " + mEmployesClone.get(i).getPrenom().toLowerCase()).contains(newText.toLowerCase()))
                        mEmployes.add(mEmployesClone.get(i));
                }

                mEmployesListAdapter.notifyDataSetChanged();

                return false;
            }
        });


        // Récupérer la liste des employés
        EmployesListTask employesListTask = new EmployesListTask(mHandler, EmployesFragment.this);
        employesListTask.start();

        // Hide virtual keyboard when touch the recyclerView
        mEmployesList.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mSearch.clearFocus();
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(MainActivity.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
                return false;
            }
        });

        return v;
    }

    private void bindActivity(View v) {
        mEmployesList = (RecyclerView) v.findViewById(R.id.employes_list_id);
        mSearch = (SearchView) v.findViewById(R.id.employes_search_id);
        mAddEmploye = (FloatingActionButton) v.findViewById(R.id.employes_add_id);

        mEmployesList.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    // cas du succée pour la récupération du liste des employés
    @Override
    public void onSucceed(ArrayList<Employe> employes) {

        this.mEmployes = employes;
        mEmployesClone = (ArrayList<Employe>) employes.clone();
        mEmployesListAdapter = new EmployesListAdapter(getContext(), employes);
        mEmployesList.setAdapter(mEmployesListAdapter);

    }

    @Override
    public void onFail(ArrayList<Employe> employes) {

        mEmployesListAdapter = new EmployesListAdapter(getContext(), employes);
        mEmployesList.setAdapter(mEmployesListAdapter);
    }


}
