package com.example.bouhlel.anypli.ui.activities;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.adapters.HistoriquesAppareilAdapter;
import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.models.HistoriqueAppareil;
import com.example.bouhlel.anypli.tasks.HistoriquesAppareilListTask;
import com.example.bouhlel.anypli.tasks.SearchHistoriquesAppareilTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


/*
*
* Afficher la liste des historiques de l'appareil
*
 */
public class HistoriqueAppareilActivity extends AppCompatActivity implements HistoriquesAppareilListTask.HistoriquesAppareilListListener, SearchHistoriquesAppareilTask.SearchHistoriquesAppareilListener {

    private EditText mDateDebut;
    private EditText mDateFin;
    private ImageButton mFiltrer;
    private ImageButton mCancel;
    private RecyclerView mHistoriquesList;

    private ArrayList<HistoriqueAppareil> historiquesAppareil;
    private HistoriquesAppareilAdapter historiquesAppareilAdapter;

    private String appareil_id;
    private HashMap<String, String> mParams;

    private DatePickerDialog datePickerDialog;

    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historique_appareil);

        Bundle bundle = getIntent().getExtras();
        appareil_id = String.valueOf(bundle.getInt("appareil_id"));

        bindActivity();

        historiquesAppareil = new ArrayList<>();
        mParams = new HashMap<>();
        mHandler = new Handler();

        mCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Récupérer la liste des historiques
                HistoriquesAppareilListTask historiquesListTask = new HistoriquesAppareilListTask(mHandler, HistoriqueAppareilActivity.this, appareil_id);
                historiquesListTask.start();
            }
        });

        mFiltrer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mParams.put("appareil_id", appareil_id);
                mParams.put("dateDebut", mDateDebut.getText().toString());
                mParams.put("dateFin", mDateFin.getText().toString());

                // Filtrage selon les dates début et fin
                SearchHistoriquesAppareilTask searchHistoriquesTask = new SearchHistoriquesAppareilTask(mHandler, HistoriqueAppareilActivity.this, mParams);
                searchHistoriquesTask.start();
            }
        });


        // Récupérer la liste des historiques
        HistoriquesAppareilListTask historiquesListTask = new HistoriquesAppareilListTask(mHandler, HistoriqueAppareilActivity.this, appareil_id);
        historiquesListTask.start();

        mDateDebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                datePickerDialog = new DatePickerDialog(HistoriqueAppareilActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                mDateDebut.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        mDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(HistoriqueAppareilActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                if (Outils.CheckDates(mDateDebut.getText().toString(), date)) {

                                    mDateFin.setText(date);

                                } else {
                                    Toast.makeText(HistoriqueAppareilActivity.this, getResources().getString(R.string.invalide_date_msg), Toast.LENGTH_SHORT).show();
                                }


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

    }

    private void bindActivity() {
        mDateDebut = (EditText) findViewById(R.id.historiques_date_debut_id);
        mDateFin = (EditText) findViewById(R.id.historiques_date_fin_id);
        mFiltrer = (ImageButton) findViewById(R.id.historiques_filter_id);
        mCancel = (ImageButton) findViewById(R.id.historiques_cancel_id);
        mHistoriquesList = (RecyclerView) findViewById(R.id.historiques_list_id);

        mHistoriquesList.setLayoutManager(new LinearLayoutManager(this));
    }

    // Passer la liste des historiques à l'adapter
    @Override
    public void onSucceed(ArrayList<HistoriqueAppareil> historiques) {

        historiquesAppareil = historiques;

        mDateDebut.setText("");
        mDateFin.setText("");

        historiquesAppareilAdapter = new HistoriquesAppareilAdapter(HistoriqueAppareilActivity.this, historiquesAppareil);
        mHistoriquesList.setAdapter(historiquesAppareilAdapter);
        historiquesAppareilAdapter.notifyDataSetChanged();
    }

    @Override
    public void onFail(ArrayList<HistoriqueAppareil> historiques) {

        historiquesAppareil = historiques;
        historiquesAppareilAdapter = new HistoriquesAppareilAdapter(HistoriqueAppareilActivity.this, historiquesAppareil);
        mHistoriquesList.setAdapter(historiquesAppareilAdapter);
    }

    @Override
    public void onSearchSuccess(ArrayList<HistoriqueAppareil> historiques) {

        historiquesAppareil = historiques;
        historiquesAppareilAdapter = new HistoriquesAppareilAdapter(HistoriqueAppareilActivity.this, historiquesAppareil);
        mHistoriquesList.setAdapter(historiquesAppareilAdapter);
        historiquesAppareilAdapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchFail(ArrayList<HistoriqueAppareil> historiques) {
        historiquesAppareil = historiques;
        historiquesAppareilAdapter = new HistoriquesAppareilAdapter(HistoriqueAppareilActivity.this, historiquesAppareil);
        mHistoriquesList.setAdapter(historiquesAppareilAdapter);
        historiquesAppareilAdapter.notifyDataSetChanged();
    }

}
