package com.example.bouhlel.anypli.models;

/**
 * Created by bouhlel on 26/07/17.
 */

public class HistoriqueEmploye {

    private int historique_id;
    private String categorie;
    private String marque;
    private String reference;
    private String dateDebut;
    private String dateFin;
    private int appareil_id;

    public HistoriqueEmploye() {
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public int getHistorique_id() {
        return historique_id;
    }

    public void setHistorique_id(int historique_id) {
        this.historique_id = historique_id;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public int getAppareil_id() {
        return appareil_id;
    }

    public void setAppareil_id(int appareil_id) {
        this.appareil_id = appareil_id;
    }
}
