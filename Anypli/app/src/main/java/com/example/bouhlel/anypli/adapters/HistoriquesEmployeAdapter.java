package com.example.bouhlel.anypli.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.holders.HistoriquesEmployeHolder;
import com.example.bouhlel.anypli.holders.ItemClickListener;
import com.example.bouhlel.anypli.models.HistoriqueEmploye;
import com.example.bouhlel.anypli.tasks.DeleteHistoriqueEmployeTask;

import java.util.ArrayList;

/**
 * Created by bouhlel on 26/07/17.
 */

public class HistoriquesEmployeAdapter extends RecyclerView.Adapter<HistoriquesEmployeHolder> implements DeleteHistoriqueEmployeTask.DeleteHistoriqueEmployeListener {

    private Context context;
    private ArrayList<HistoriqueEmploye> historiques;
    private Handler mHandler;


    public HistoriquesEmployeAdapter(Context context, ArrayList<HistoriqueEmploye> historiques) {
        this.context = context;
        this.historiques = historiques;
        mHandler = new Handler();
    }

    @Override
    public HistoriquesEmployeHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.employe_historiques_row, parent, false);
        return new HistoriquesEmployeHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoriquesEmployeHolder holder, int position) {

        final HistoriqueEmploye historique = historiques.get(position);
        holder.bind(historique);
        holder.setClickListener(new ItemClickListener() {

            @Override
            public void onClick(View view, final int position, boolean isLongClick) {

                if (isLongClick) {

                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    // Supprimer un élément de l'historique
                                    DeleteHistoriqueEmployeTask deleteHistorique = new DeleteHistoriqueEmployeTask(mHandler, HistoriquesEmployeAdapter.this, String.valueOf(historique.getHistorique_id()), position);
                                    deleteHistorique.start();

                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getString(R.string.delete_historique_msg)).setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return historiques.size();
    }

    @Override
    public void onDeleteSuccess(int position) {
        historiques.remove(position);
        notifyItemRemoved(position);
        Toast.makeText(context, context.getResources().getString(R.string.delete_historique_success_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteFail() {
        Toast.makeText(context, context.getResources().getString(R.string.delete_historique_fail_msg), Toast.LENGTH_SHORT).show();
    }

}
