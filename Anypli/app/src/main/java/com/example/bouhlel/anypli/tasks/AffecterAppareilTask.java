package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/*
*
* Affécter une appareil à un employé
*
 */

public class AffecterAppareilTask extends Thread {

    private Handler mHandler;
    private AffecterAppareilListener mAffecterAppareilListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;

    public AffecterAppareilTask(Handler mHandler, AffecterAppareilListener mAffecterAppareilListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mAffecterAppareilListener = mAffecterAppareilListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (affecterAppareilService())
            onSuccess();
        else
            onFail();

    }


    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAffecterAppareilListener.onAffecterFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAffecterAppareilListener.onAffecterSuccess();
            }
        });
    }


    public boolean affecterAppareilService() {

        String url_server = Outils.API_SERVER + "historique";

        RequestBody body = new FormBody.Builder()
                .add("appareil_id", mParams.get("appareil_id"))
                .add("employe_id", mParams.get("employe_id"))
                .add("debut", mParams.get("dateDebut"))
                .add("fin", mParams.get("dateFin"))
                .build();

        try {

            Response response = ApiCall.POST(mClient, url_server, body); // appel du web service pour l'affectation de l'appareil
            int httpStatusCode = response.code();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }


    public interface AffecterAppareilListener {

        void onAffecterSuccess();

        void onAffecterFail();

    }
}
