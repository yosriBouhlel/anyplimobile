package com.example.bouhlel.anypli.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.adapters.CategorieSpinnerAdapter;
import com.example.bouhlel.anypli.adapters.HistoriquesEmployeAdapter;
import com.example.bouhlel.anypli.models.HistoriqueEmploye;
import com.example.bouhlel.anypli.tasks.HistoriquesEmployeListTask;

import java.util.ArrayList;

/*
*
* Afficher la liste des historiques de l'employé
*
 */
public class HistoriqueEmployeActivity extends AppCompatActivity implements HistoriquesEmployeListTask.HistoriquesEmployeListListener {

    private Spinner mCategorie;
    private RecyclerView mHistoriquesList;
    private SearchView mSearch;

    private String employe_id;
    private ArrayList<HistoriqueEmploye> mHistoriqueEmployes;
    private ArrayList<HistoriqueEmploye> mHistoriqueEmployesClone;
    private HistoriquesEmployeAdapter mHistoriquesEmployeAdapter;

    private Integer[] categories = new Integer[]{R.drawable.ic_list_appareil, R.drawable.ic_smartphone, R.drawable.ic_pc, R.drawable.ic_tablet};
    private CategorieSpinnerAdapter categorieSpinnerAdapter;

    private Handler mHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historique_employe);

        Bundle bundle = getIntent().getExtras();
        employe_id = String.valueOf(bundle.getInt("employe_id"));

        mHistoriqueEmployes = new ArrayList<>();
        mHandler = new Handler();

        bindActivity();

        categorieSpinnerAdapter = new CategorieSpinnerAdapter(categories, HistoriqueEmployeActivity.this);
        mCategorie.setAdapter(categorieSpinnerAdapter);

        mCategorie.setSelection(0);

        mCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String choix = "all";

                mHistoriqueEmployes.clear();

                if (position == 0) {

                    // Récupérer la liste des historiques
                    HistoriquesEmployeListTask historiquesListTask = new HistoriquesEmployeListTask(mHandler, HistoriqueEmployeActivity.this, employe_id);
                    historiquesListTask.start();

                } else {

                    switch (position) {
                        case 1:
                            choix = "Smartphones";
                            break;
                        case 2:
                            choix = "Ordinateurs";
                            break;
                        case 3:
                            choix = "Tablettes";
                            break;
                        default:
                            break;
                    }


                    for (int i = 0; i < mHistoriqueEmployesClone.size(); i++) {
                        if (mHistoriqueEmployesClone.get(i).getCategorie().equals(choix))
                            mHistoriqueEmployes.add(mHistoriqueEmployesClone.get(i));
                    }


                }

                mHistoriquesEmployeAdapter = new HistoriquesEmployeAdapter(HistoriqueEmployeActivity.this, mHistoriqueEmployes);
                mHistoriquesList.setAdapter(mHistoriquesEmployeAdapter);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        mSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            // Filtrage selon la marque et la catégorie de l'appareil
            @Override
            public boolean onQueryTextChange(String newText) {

                mHistoriqueEmployes.clear();

                if (mCategorie.getSelectedItemPosition() == 0) {

                    for (int i = 0; i < mHistoriqueEmployesClone.size(); i++) {
                        if ((mHistoriqueEmployesClone.get(i).getMarque().toLowerCase() + " " + mHistoriqueEmployesClone.get(i).getReference().toLowerCase()).contains(newText.toLowerCase()))
                            mHistoriqueEmployes.add(mHistoriqueEmployesClone.get(i));
                    }

                    mHistoriquesEmployeAdapter.notifyDataSetChanged();

                } else {
                    String choix = "Smartphones";

                    switch (mCategorie.getSelectedItemPosition()) {
                        case 1:
                            choix = "Smartphones";
                            break;
                        case 2:
                            choix = "Ordinateurs";
                            break;
                        case 3:
                            choix = "Tablettes";
                            break;
                        default:
                            break;
                    }

                    for (int i = 0; i < mHistoriqueEmployesClone.size(); i++) {

                        if (mHistoriqueEmployesClone.get(i).getCategorie().equals(choix) && (mHistoriqueEmployesClone.get(i).getMarque().toLowerCase() + " " + mHistoriqueEmployesClone.get(i).getReference().toLowerCase()).contains(newText.toLowerCase()))
                            mHistoriqueEmployes.add(mHistoriqueEmployesClone.get(i));
                    }

                    mHistoriquesEmployeAdapter.notifyDataSetChanged();

                }
                return false;
            }
        });

    }

    private void bindActivity() {
        mCategorie = (Spinner) findViewById(R.id.historique_employe_categorie_id);
        mSearch = (SearchView) findViewById(R.id.historique_employe_search_id);
        mHistoriquesList = (RecyclerView) findViewById(R.id.historique_employe_list_id);
        mHistoriquesList.setLayoutManager(new LinearLayoutManager(this));
    }


    // Hide virtual keyboard when touch out side EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    // Passer la liste des historiques à l'adapter
    @Override
    public void onSucceed(ArrayList<HistoriqueEmploye> historiqueEmployes) {
        mCategorie.setSelection(0);
        this.mHistoriqueEmployes = historiqueEmployes;
        mHistoriqueEmployesClone = (ArrayList<HistoriqueEmploye>) historiqueEmployes.clone();
        mHistoriquesEmployeAdapter = new HistoriquesEmployeAdapter(HistoriqueEmployeActivity.this, historiqueEmployes);
        mHistoriquesList.setAdapter(mHistoriquesEmployeAdapter);
    }

    @Override
    public void onFail(ArrayList<HistoriqueEmploye> historiqueEmployes) {

        this.mHistoriqueEmployes = historiqueEmployes;
        mHistoriquesEmployeAdapter = new HistoriquesEmployeAdapter(HistoriqueEmployeActivity.this, historiqueEmployes);
        mHistoriquesList.setAdapter(mHistoriquesEmployeAdapter);
    }

}
