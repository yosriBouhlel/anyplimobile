package com.example.bouhlel.anypli.api;

import java.io.File;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by bouhlel on 18/07/17.
 */

public class ApiCall {

    //GET network request
    public static Response GET(OkHttpClient client, String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();

        return response;
    }

    //POST network request
    public static Response POST(OkHttpClient client, String url, RequestBody body) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }

    //DELETE network request
    public static Response DELETE(OkHttpClient client, String url, String token) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .delete()
                .header("Authorization", "bearer " + token)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }

    //PUT network request
    public static Response PUT(OkHttpClient client, String url, RequestBody body, String token) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .header("Authorization", "bearer " + token)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }


    //PUT network request
    public static Response updateAppareil(OkHttpClient client, String url, RequestBody body) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .put(body)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }

    //DELETE an appareil network request
    public static Response DeleteAppareil(OkHttpClient client, String url, RequestBody body) throws IOException {

        Request request = new Request.Builder()
                .url(url)
                .delete(body)
                .build();

        Response response = client.newCall(request).execute();
        return response;
    }



    //Upload an image to the server
    public static MultipartBody uploadRequestBody(String title, String imageFormat, File file) {

        MediaType MEDIA_TYPE = MediaType.parse("image/" + imageFormat); // e.g. "image/png"
        return new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("action", "upload")
                .addFormDataPart("format", "json")
                .addFormDataPart("filename", title + "." + imageFormat) //e.g. title.png --> imageFormat = png
                .addFormDataPart("file", "...", RequestBody.create(MEDIA_TYPE, file))
                .build();
    }


}
