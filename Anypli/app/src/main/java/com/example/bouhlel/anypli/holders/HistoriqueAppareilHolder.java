package com.example.bouhlel.anypli.holders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.HistoriqueAppareil;

/**
 * Created by bouhlel on 07/09/17.
 */

public class HistoriqueAppareilHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

    private TextView mEmploye;
    private TextView mDateDebut;
    private TextView mDateFin;

    private ItemClickListener mClickListener;


    public HistoriqueAppareilHolder(View itemView) {
        super(itemView);

        mEmploye = (TextView) itemView.findViewById(R.id.historique_employe_name_id);
        mDateDebut = (TextView) itemView.findViewById(R.id.historique_date_debut_id);
        mDateFin = (TextView) itemView.findViewById(R.id.historique_date_fin_id);

        itemView.setOnClickListener(this);
        itemView.setOnLongClickListener(this);
    }

    public void bind(HistoriqueAppareil historique) {

        if (!historique.getDateFin().equals("null")) {
            mDateFin.setText(historique.getDateFin());
        } else
            mDateFin.setText("n\'est pas spécifié");

        mEmploye.setText(historique.getNomEmploye());
        mDateDebut.setText(historique.getDateDebut());

    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {

        mClickListener.onClick(v, getPosition(), false);
    }

    @Override
    public boolean onLongClick(View v) {

        mClickListener.onClick(v, getPosition(), true);

        return true;
    }
}
