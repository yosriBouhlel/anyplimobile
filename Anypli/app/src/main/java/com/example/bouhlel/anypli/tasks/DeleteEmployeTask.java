package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour supprimer un employé
 */

public class DeleteEmployeTask extends Thread {

    private Handler mHandler;
    private DeleteEmployeListener mDeleteEmployeListener;
    private String mEmploye_id;
    private OkHttpClient mClient;
    private int mPosition;


    public DeleteEmployeTask(Handler mHandler, DeleteEmployeListener mDeleteEmployeListener, String employe_id, int position) {

        this.mHandler = mHandler;
        this.mDeleteEmployeListener = mDeleteEmployeListener;
        this.mEmploye_id = employe_id;
        this.mPosition = position;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (deleteEmployeService(mEmploye_id))
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteEmployeListener.onDeleteFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mDeleteEmployeListener.onDeleteSuccess(mPosition);
            }
        });
    }

    public boolean deleteEmployeService(String employe_id) {

        String url_server = Outils.API_SERVER + "employes/delete";

        try {

            RequestBody body = new FormBody.Builder()
                    .add("id", employe_id)
                    .build();

            Response response = ApiCall.DeleteAppareil(mClient, url_server, body);// appel du web service pour la suppression d'un employé

            int httpStatusCode = response.code();
            response.body().close();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }

    public interface DeleteEmployeListener {

        void onDeleteSuccess(int position);

        void onDeleteFail();

    }
}
