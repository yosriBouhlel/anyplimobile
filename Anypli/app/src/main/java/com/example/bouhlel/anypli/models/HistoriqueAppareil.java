package com.example.bouhlel.anypli.models;

/**
 * Created by bouhlel on 24/07/17.
 */

public class HistoriqueAppareil {

    private int historique_id;
    private String nomEmploye;
    private String dateDebut;
    private String dateFin;
    private int employe_id;

    public HistoriqueAppareil() {
    }

    public int getHistorique_id() {
        return historique_id;
    }

    public void setHistorique_id(int historique_id) {
        this.historique_id = historique_id;
    }

    public String getNomEmploye() {
        return nomEmploye;
    }

    public void setNomEmploye(String nomEmploye) {
        this.nomEmploye = nomEmploye;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public int getEmploye_id() {
        return employe_id;
    }

    public void setEmploye_id(int employe_id) {
        this.employe_id = employe_id;
    }
}
