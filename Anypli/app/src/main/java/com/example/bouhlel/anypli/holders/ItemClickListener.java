package com.example.bouhlel.anypli.holders;

import android.view.View;

/**
 * Created by bouhlel on 06/09/17.
 */

public interface ItemClickListener {

    void onClick(View view, int position, boolean isLongClick);
}
