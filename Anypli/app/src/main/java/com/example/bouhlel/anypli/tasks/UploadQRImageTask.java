package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.File;
import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour l'upload du QR code au serveur
 */

public class UploadQRImageTask extends Thread {

    private Handler mHandler;
    private UploadQRImageListener mUploadQRImageListener;
    private String mTitle;
    private OkHttpClient mClient;
    private File mImageFile;

    public UploadQRImageTask(Handler mHandler, UploadQRImageListener mUploadQRImageListener, String title, File imageFile) {

        this.mImageFile = imageFile;
        this.mHandler = mHandler;
        this.mUploadQRImageListener = mUploadQRImageListener;
        this.mTitle = title;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (uploadImageService(mTitle))
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUploadQRImageListener.onUploadFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUploadQRImageListener.onUploadSuccess();
            }
        });
    }


    public boolean uploadImageService(String title) {

        String url_server = Outils.API_SERVER + "appareil/upload";

        try {

            Response response = ApiCall.POST(mClient, url_server, ApiCall.uploadRequestBody(title, "png", mImageFile));// appel du web service pour l'upload du QR code au serveur
            int httpStatusCode = response.code();
            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface UploadQRImageListener {

        void onUploadSuccess();

        void onUploadFail();

    }
}
