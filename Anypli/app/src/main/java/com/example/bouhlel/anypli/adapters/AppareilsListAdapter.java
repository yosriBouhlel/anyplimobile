package com.example.bouhlel.anypli.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.holders.AppareilsHolder;
import com.example.bouhlel.anypli.holders.ItemClickListener;
import com.example.bouhlel.anypli.models.Appareil;
import com.example.bouhlel.anypli.tasks.DeleteAppareilTask;
import com.example.bouhlel.anypli.ui.activities.AppareilDetailsActivity;

import java.util.ArrayList;

/**
 * Created by bouhlel on 20/07/17.
 */

public class AppareilsListAdapter extends RecyclerView.Adapter<AppareilsHolder> implements DeleteAppareilTask.DeleteAppareilListener {

    private Context context;
    private ArrayList<Appareil> appareils;
    private Handler mHandler;


    public AppareilsListAdapter(Context context, ArrayList<Appareil> appareils) {
        this.context = context;
        this.appareils = appareils;
        mHandler = new Handler();
    }

    @Override
    public AppareilsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.appareils_list_row, parent, false);

        return new AppareilsHolder(view);
    }

    @Override
    public void onBindViewHolder(AppareilsHolder holder, int position) {

        final Appareil appareil = appareils.get(position);

        holder.bind(appareil);

        holder.setClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, final int position, boolean isLongClick) {

                if (isLongClick) {
                    // Demander la confirmation pour supprimer un employé
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:

                                    // supprimer un employé
                                    DeleteAppareilTask deleteAppareilTask = new DeleteAppareilTask(mHandler, AppareilsListAdapter.this, String.valueOf(appareil.getAppareil_id()), position);
                                    deleteAppareilTask.start();
                                    break;

                                case DialogInterface.BUTTON_NEGATIVE:
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setMessage(context.getResources().getString(R.string.delete_appareil_msg)).setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();

                } else {

                    // vers les caractéristiques de l'appareil
                    Intent goDetailsAppareil = new Intent(context, AppareilDetailsActivity.class);
                    goDetailsAppareil.putExtra("appareil_id", appareil.getAppareil_id());
                    context.startActivity(goDetailsAppareil);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return appareils.size();
    }

    // cas de succée pour la supression d'une appareil
    @Override
    public void onDeleteSuccess(int position) {

        appareils.remove(position);
        notifyItemRemoved(position);

        Toast.makeText(context, context.getResources().getString(R.string.delete_appareil_success_msg), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteFail() {

        Toast.makeText(context, context.getResources().getString(R.string.delete_appareil_fail_msg), Toast.LENGTH_SHORT).show();

    }

}
