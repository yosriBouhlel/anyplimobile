package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Thread pour mettre à jour la disponibilité de l'appareil
 */

public class UpdateAppareilTask extends Thread {

    private Handler mHandler;
    private UpdateAppareilListener mUpdateAppareilListener;
    private int mAppareil_id;
    private OkHttpClient mClient;

    public UpdateAppareilTask(Handler mHandler, UpdateAppareilListener mUpdateAppareilListener, int appareil_id) {

        this.mHandler = mHandler;
        this.mUpdateAppareilListener = mUpdateAppareilListener;
        this.mAppareil_id = appareil_id;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (updateAppareilService(mAppareil_id))
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateAppareilListener.onUpdateFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mUpdateAppareilListener.onUpdateSuccess();
            }
        });
    }

    public boolean updateAppareilService(int appareil_id) {

        String url_server = Outils.API_SERVER + "appareil/update";

        try {

            RequestBody body = new FormBody.Builder()
                    .add("id", String.valueOf(appareil_id))
                    .build();

            Response response = ApiCall.updateAppareil(mClient, url_server, body); // appel du web service pour mettre à jour la disponibilité de l'appareil
            int httpStatusCode = response.code();
            response.body().close();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;


        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }


    public interface UpdateAppareilListener {

        void onUpdateSuccess();

        void onUpdateFail();

    }
}
