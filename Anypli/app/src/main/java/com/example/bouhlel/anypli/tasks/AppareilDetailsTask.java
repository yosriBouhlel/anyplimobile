package com.example.bouhlel.anypli.tasks;

import android.content.Context;
import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.Appareil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/*
*
* Thread pour récupérer les caractéristiques d'une appareil
*
 */

public class AppareilDetailsTask extends Thread {

    Context mContext;
    private Handler mHandler;
    private AppareilDetailsListener mAppareilDetailsListener;
    private int appareil_id;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private Appareil mAppareil;
    private String mEmployeName;

    public AppareilDetailsTask(Handler mHandler, AppareilDetailsListener mAppareilDetailsListener, Context mContext, int appareil_id) {

        this.mContext = mContext;
        this.mHandler = mHandler;
        this.mAppareilDetailsListener = mAppareilDetailsListener;
        this.appareil_id = appareil_id;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        appareilDetailsService(appareil_id);

        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilDetailsListener.onFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilDetailsListener.onSucceed(mAppareil, mEmployeName);
            }
        });
    }


    public void appareilDetailsService(int appareil_id) {

        String url_server = Outils.API_SERVER + "appareils/show?appareil_id=" + appareil_id;

        try {

            Response response = ApiCall.GET(mClient, url_server); // appel du web service pour récupérer les caractéristiques de l'appareil
            mHttpStatusCode = response.code();

            JSONObject object = new JSONObject(response.body().string());
            JSONObject appareilObject = (JSONObject) object.get("result");

            mAppareil = new Appareil();

            mAppareil.setAppareil_id(appareilObject.getInt("id"));
            mAppareil.setCategorie(appareilObject.getString("categorie"));
            mAppareil.setMarque(appareilObject.getString("marque"));
            mAppareil.setReference(appareilObject.getString("reference"));

            if (!appareilObject.getString("microprocesseur").equals("null"))
                mAppareil.setMicroprocesseur(appareilObject.getString("microprocesseur"));
            else
                mAppareil.setMicroprocesseur(mContext.getResources().getString(R.string.pas_specifie_msg));

            if (!appareilObject.isNull("cache"))
                mAppareil.setCache(appareilObject.getInt("cache"));

            if (!appareilObject.isNull("ram"))
                mAppareil.setRam(appareilObject.getInt("ram"));
            if (!appareilObject.isNull("disque_dur"))
                mAppareil.setDisqueDur(appareilObject.getInt("disque_dur"));

            if (!appareilObject.getString("os").equals("null"))
                mAppareil.setOs(appareilObject.getString("os"));
            else
                mAppareil.setOs(mContext.getResources().getString(R.string.pas_specifie_msg));

            mAppareil.setQrcode(appareilObject.getString("qrcode"));
            mAppareil.setDisponible(appareilObject.getInt("disponible"));
            mEmployeName = (String) object.get("name");

        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public interface AppareilDetailsListener {

        void onSucceed(Appareil appareil, String employeName);

        void onFail();

    }
}
