package com.example.bouhlel.anypli.global;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by bouhlel on 17/07/17.
 */

public class Outils {

    public static final String API_SERVER = "http://192.168.1.3:8000/api/";

    public static final String SERVER = "http://192.168.1.3:8000";

    public static final String UPLOADS_SERVER = "/downloads/";

    public static boolean CheckDates(String debut, String fin) {
        SimpleDateFormat dfDate  = new SimpleDateFormat("yyyy-MM-dd");
        boolean b = false;
        try {
            if(dfDate.parse(debut).before(dfDate.parse(fin)))
            {
                b = true;//If start date is before end date
            }
            else if(dfDate.parse(debut).equals(dfDate.parse(fin)))
            {
                b = true;//If two dates are equal
            }
            else
            {
                b = false; //If start date is after the end date
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return b;
    }


}
