package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/*
*
* Thread pour vérifier l'existance d'une appareil dans la BD
*
 */

public class AppareilExistTask extends Thread {

    private Handler mHandler;
    private AppareilExistListener mAppareilExistListener;
    private String mAppareil_id;
    private OkHttpClient mClient;

    public AppareilExistTask(Handler mHandler, AppareilExistListener mAppareilExistListener, String mAppareil_id) {

        this.mHandler = mHandler;
        this.mAppareilExistListener = mAppareilExistListener;
        this.mAppareil_id = mAppareil_id;
        mClient = new OkHttpClient();

    }

    @Override
    public void run() {
        super.run();

        if (AppareilExistService())
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilExistListener.onFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilExistListener.onSucceed();
            }
        });
    }

    public boolean AppareilExistService() {

        String url_server = Outils.API_SERVER + "appareil/exist?appareil_id=" + mAppareil_id;

        try {
            Response response = ApiCall.GET(mClient, url_server); // appel du web service pour la vérification de l'existance d'une appareil dans la BD
            int httpStatusCode = response.code();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public interface AppareilExistListener {

        void onSucceed();

        void onFail();

    }
}
