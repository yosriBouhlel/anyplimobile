package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.Employe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour récupérer la liste des employés
 */

public class EmployesListTask extends Thread {

    private Handler mHandler;
    private EmployesListListener mEmployesListListener;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private ArrayList<Employe> mEmployes;

    public EmployesListTask(Handler mHandler, EmployesListListener mEmployesListListener) {

        this.mHandler = mHandler;
        this.mEmployesListListener = mEmployesListListener;
        mClient = new OkHttpClient();
        mEmployes = new ArrayList<>();

    }

    @Override
    public void run() {
        super.run();

        employesListService();
        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mEmployesListListener.onFail(mEmployes);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mEmployesListListener.onSucceed(mEmployes);
            }
        });
    }


    public void employesListService() {

        String url_server = Outils.API_SERVER + "employes";

        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour récupérer la liste des employés
            mHttpStatusCode = response.code();

            JSONObject employesObject = new JSONObject(response.body().string());
            JSONArray employesArray = employesObject.getJSONArray("result");

            for (int i = 0; i < employesArray.length(); i++) {
                JSONObject employeObject = employesArray.getJSONObject(i);
                Employe employe = new Employe();

                employe.setEmploye_id(employeObject.getInt("id"));
                employe.setNom(employeObject.getString("nom"));
                employe.setPrenom(employeObject.getString("prenom"));
                employe.setEmail(employeObject.getString("email"));
                employe.setSpecialite(employeObject.getString("specialite"));
                employe.setTel(employeObject.getString("tel"));

                mEmployes.add(employe);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface EmployesListListener {

        void onSucceed(ArrayList<Employe> employes);

        void onFail(ArrayList<Employe> employes);

    }
}
