package com.example.bouhlel.anypli.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.global.SessionManager;

public class SplashActivity extends AppCompatActivity {

    private static final int SPLASH_TIME_OUT = 1000;

    SessionManager mSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mSession = SessionManager.getInstance(getApplicationContext());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if (mSession.isLoggedIn()) {

                    // accéder directement à l'interface d'acceuil s'il est déja connecté
                    Intent goHome = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(goHome);
                    finish();

                } else {

                    Intent login = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(login);
                    finish();

                }

            }
        }, SPLASH_TIME_OUT);


    }

}
