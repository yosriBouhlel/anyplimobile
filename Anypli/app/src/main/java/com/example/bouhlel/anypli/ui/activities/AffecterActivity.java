package com.example.bouhlel.anypli.ui.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Employe;
import com.example.bouhlel.anypli.tasks.AffecterAppareilTask;
import com.example.bouhlel.anypli.tasks.EmployesListTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;


/*
*
* Affecter une appareil à un employé en spéçifiant les dates début et fin ou bien seulement la date de début d'affectation
*
 */

public class AffecterActivity extends AppCompatActivity implements EmployesListTask.EmployesListListener, AffecterAppareilTask.AffecterAppareilListener {


    private Spinner mlisteEmploye;
    private EditText mDateDebut;
    private EditText mDateFin;
    private Button mAffecterBtn;

    private int appareil_id = 0;
    private int employe_id = 0;
    private ArrayList<Employe> employes;
    private ArrayList<String> employesNames;
    private HashMap<String, String> mParams;

    private Handler mHandler;

    private DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affecter);

        Bundle bundle = getIntent().getExtras();
        appareil_id = bundle.getInt("appareil_id");

        bindActivity();

        employes = new ArrayList<>();
        mParams = new HashMap<>();
        mHandler = new Handler();

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mAffecterBtn.setTypeface(face);

        // Thread pour récupérer la liste de tout les employés
        EmployesListTask employesListTask = new EmployesListTask(mHandler, AffecterActivity.this);
        employesListTask.start();


        mlisteEmploye.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                employe_id = employes.get(position).getEmploye_id();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // choisir la date de début d'affectation de l'appareil
        mDateDebut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);

                datePickerDialog = new DatePickerDialog(AffecterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                mDateDebut.setText(year + "-" + (monthOfYear + 1) + "-"
                                        + dayOfMonth);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });

        // choisir la date de fin d'affectation de l'appareil
        mDateFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day

                datePickerDialog = new DatePickerDialog(AffecterActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                String date = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;

                                if (Outils.CheckDates(mDateDebut.getText().toString(), date)) {

                                    mDateFin.setText(date);

                                } else {
                                    Toast.makeText(AffecterActivity.this, getResources().getString(R.string.invalide_date_msg), Toast.LENGTH_SHORT).show();
                                }


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });


        mAffecterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mDateDebut.getText().toString().isEmpty())
                    Toast.makeText(AffecterActivity.this, getResources().getString(R.string.affecter_appareil_date_msg), Toast.LENGTH_LONG).show();
                else {

                    mParams.put("appareil_id", String.valueOf(appareil_id));
                    mParams.put("employe_id", String.valueOf(employe_id));
                    mParams.put("dateDebut", mDateDebut.getText().toString());
                    mParams.put("dateFin", mDateFin.getText().toString());

                    AffecterAppareilTask affecterAppareilTask = new AffecterAppareilTask(mHandler, AffecterActivity.this, mParams);
                    affecterAppareilTask.start();

                }
            }
        });


    }

    private void bindActivity() {
        mlisteEmploye = (Spinner) findViewById(R.id.employe_spinner_id);
        mDateDebut = (EditText) findViewById(R.id.date_debut_id);
        mDateFin = (EditText) findViewById(R.id.date_fin_id);
        mAffecterBtn = (Button) findViewById(R.id.affecter_btn_id);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                setResult(RESULT_CANCELED);
                finish();
                return false;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    // Passer la liste des noms des employés à l'adapter du spinner
    @Override
    public void onSucceed(ArrayList<Employe> employes) {

        this.employes = employes;

        employesNames = new ArrayList<>();
        for (int i = 0; i < employes.size(); i++)
            employesNames.add(employes.get(i).getNom() + " " + employes.get(i).getPrenom());


        ArrayAdapter<String> adapter = new ArrayAdapter<>(AffecterActivity.this, android.R.layout.simple_spinner_item, employesNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mlisteEmploye.setAdapter(adapter);
    }

    @Override
    public void onFail(ArrayList<Employe> employes) {

    }


    // Redireger vers caracteristiques de l'appareil dans le cas de succées de l'affectation
    @Override
    public void onAffecterSuccess() {
        Toast.makeText(AffecterActivity.this, getResources().getString(R.string.affecter_appareil_success_msg), Toast.LENGTH_LONG).show();
        Intent backToDetails = new Intent();
        backToDetails.putExtra("employeName", mlisteEmploye.getSelectedItem().toString());
        setResult(RESULT_OK, backToDetails);
        finish();
    }

    @Override
    public void onAffecterFail() {
        Toast.makeText(AffecterActivity.this, getResources().getString(R.string.affecter_appareil_fail_msg), Toast.LENGTH_LONG).show();
    }

}
