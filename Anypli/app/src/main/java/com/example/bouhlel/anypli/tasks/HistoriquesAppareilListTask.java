package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.HistoriqueAppareil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/**
 * Thread pour récupérer la liste des historiques de l'appareil
 */

public class HistoriquesAppareilListTask extends Thread {

    private Handler mHandler;
    private HistoriquesAppareilListListener mHistoriquesAppareilListListener;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private String mAppareil_id;
    private ArrayList<HistoriqueAppareil> mHistoriquesAppareil;


    public HistoriquesAppareilListTask(Handler mHandler, HistoriquesAppareilListListener mHistoriquesAppareilListListener, String appareil_id) {

        this.mHandler = mHandler;
        this.mHistoriquesAppareilListListener = mHistoriquesAppareilListListener;
        this.mAppareil_id = appareil_id;
        mClient = new OkHttpClient();
        mHistoriquesAppareil = new ArrayList<>();
    }

    @Override
    public void run() {
        super.run();

        historiquesListService(mAppareil_id);

        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();
    }


    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mHistoriquesAppareilListListener.onFail(mHistoriquesAppareil);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mHistoriquesAppareilListListener.onSucceed(mHistoriquesAppareil);
            }
        });
    }

    public void historiquesListService(String appareil_id) {

        String url_server = Outils.API_SERVER + "appareils/historiques?appareil_id=" + appareil_id;

        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour récupérer la liste des historiques
            mHttpStatusCode = response.code();

            JSONObject historiquesObject = new JSONObject(response.body().string());
            JSONArray historiquesArray = historiquesObject.getJSONArray("result");

            mHistoriquesAppareil.clear();

            for (int i = 0; i < historiquesArray.length(); i++) {
                JSONObject historiqueObject = historiquesArray.getJSONObject(i);
                HistoriqueAppareil historique = new HistoriqueAppareil();

                historique.setHistorique_id(historiqueObject.getInt("historique_id"));
                historique.setEmploye_id(historiqueObject.getInt("employe_id"));
                historique.setNomEmploye(historiqueObject.getString("name"));
                historique.setDateDebut(historiqueObject.getString("debut"));
                historique.setDateFin(historiqueObject.getString("fin"));

                mHistoriquesAppareil.add(historique);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    public interface HistoriquesAppareilListListener {

        void onSucceed(ArrayList<HistoriqueAppareil> historiques);

        void onFail(ArrayList<HistoriqueAppareil> historiques);

    }

}
