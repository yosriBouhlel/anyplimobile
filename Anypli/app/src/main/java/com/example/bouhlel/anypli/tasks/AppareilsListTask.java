package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;
import com.example.bouhlel.anypli.models.Appareil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.Response;

/*
*
* Thread pour récupérer la liste des appareils
*
 */

public class AppareilsListTask extends Thread {

    private Handler mHandler;
    private AppareilsListListener mAppareilsListListener;
    private OkHttpClient mClient;
    private int mHttpStatusCode = 0;
    private ArrayList<Appareil> mAppareils;


    public AppareilsListTask(Handler mHandler, AppareilsListListener mAppareilsListListener) {

        this.mHandler = mHandler;
        this.mAppareilsListListener = mAppareilsListListener;
        mClient = new OkHttpClient();
        mAppareils = new ArrayList<>();
    }

    @Override
    public void run() {
        super.run();

        appareilsListService();

        if (mHttpStatusCode == HttpsURLConnection.HTTP_OK)
            onSuccess();
        else
            onFail();

    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilsListListener.onFail(mAppareils);
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAppareilsListListener.onSucceed(mAppareils);
            }
        });
    }

    public void appareilsListService() {

        String url_server = Outils.API_SERVER + "appareils";


        try {

            Response response = ApiCall.GET(mClient, url_server);// appel du web service pour récupérer la liste des appareils
            mHttpStatusCode = response.code();

            JSONObject appareilsObject = new JSONObject(response.body().string());
            JSONArray appareilsArray = appareilsObject.getJSONArray("result");

            for (int i = 0; i < appareilsArray.length(); i++) {
                JSONObject appareilObject = appareilsArray.getJSONObject(i);
                Appareil appareil = new Appareil();

                appareil.setAppareil_id(appareilObject.getInt("id"));
                appareil.setCategorie(appareilObject.getString("categorie"));
                appareil.setMarque(appareilObject.getString("marque"));
                appareil.setReference(appareilObject.getString("reference"));
                if (!appareilObject.getString("microprocesseur").equals("null"))
                    appareil.setMicroprocesseur(appareilObject.getString("microprocesseur"));
                else
                    appareil.setMicroprocesseur("");
                if (!appareilObject.isNull("cache"))
                    appareil.setCache(appareilObject.getInt("cache"));
                if (!appareilObject.isNull("ram"))
                    appareil.setRam(appareilObject.getInt("ram"));
                if (!appareilObject.isNull("disque_dur"))
                    appareil.setDisqueDur(appareilObject.getInt("disque_dur"));
                appareil.setOs(appareilObject.getString("os"));
                appareil.setQrcode(appareilObject.getString("qrcode"));
                appareil.setDisponible(appareilObject.getInt("disponible"));

                mAppareils.add(appareil);
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public interface AppareilsListListener {

        void onSucceed(ArrayList<Appareil> appareils);

        void onFail(ArrayList<Appareil> appareils);

    }
}
