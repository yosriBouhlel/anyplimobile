package com.example.bouhlel.anypli.tasks;

import android.os.Handler;

import com.example.bouhlel.anypli.global.Outils;
import com.example.bouhlel.anypli.api.ApiCall;

import java.io.IOException;
import java.util.HashMap;

import javax.net.ssl.HttpsURLConnection;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;


/*
*
* Thread pour ajouter un nouveau employé
*
 */

public class AddEmployeTask extends Thread {

    private Handler mHandler;
    private AddEmployeListener mAddEmployeListener;
    private HashMap<String, String> mParams;
    private OkHttpClient mClient;

    public AddEmployeTask(Handler mHandler, AddEmployeListener mAddEmployeListener, HashMap<String, String> mParams) {

        this.mHandler = mHandler;
        this.mAddEmployeListener = mAddEmployeListener;
        this.mParams = mParams;
        mClient = new OkHttpClient();
    }

    @Override
    public void run() {
        super.run();

        if (addEmployeService(mParams))
            onSuccess();
        else
            onFail();


    }

    private void onFail() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAddEmployeListener.onFail();
            }
        });
    }

    private void onSuccess() {
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mAddEmployeListener.onSucceed();
            }
        });
    }


    public boolean addEmployeService(HashMap<String, String> coordonnees) {

        String url_server = Outils.API_SERVER + "employe";

        RequestBody body = new FormBody.Builder()
                .add("nom", coordonnees.get("firstName"))
                .add("prenom", coordonnees.get("LastName"))
                .add("specialite", coordonnees.get("specialite"))
                .add("email", coordonnees.get("email"))
                .add("tel", coordonnees.get("tel"))
                .build();

        try {

            Response response = ApiCall.POST(mClient, url_server, body); // appel du web service pour l'ajout de l'employé
            int httpStatusCode = response.code();

            if (httpStatusCode == HttpsURLConnection.HTTP_OK)
                return true;
            else
                return false;

        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;

    }

    public interface AddEmployeListener {

        void onSucceed();

        void onFail();

    }
}
