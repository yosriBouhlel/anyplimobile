package com.example.bouhlel.anypli.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.tasks.AppareilExistTask;
import com.example.bouhlel.anypli.ui.activities.AppareilDetailsActivity;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import java.io.IOException;

/*
*
* Capture du QR code
*
 */

public class DecoderFragment extends Fragment implements AppareilExistTask.AppareilExistListener {

    private final int REQUEST_CAMERA_PERMISSION_ID = 1001;

    private SurfaceView mCameraPreview;
    private BarcodeDetector mBarcodeDetector;
    private CameraSource mCameraSource;
    private SparseArray<Barcode> mQrCodes;
    private int mAppareil_id;
    private Handler mDetectionHandler;
    private Handler mHandler;

    public DecoderFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_decoder, container, false);

        mCameraPreview = (SurfaceView) v.findViewById(R.id.cameraPreview);

        mBarcodeDetector = new BarcodeDetector.Builder(getContext())
                .setBarcodeFormats(Barcode.QR_CODE)
                .build();

        mCameraSource = new CameraSource
                .Builder(getContext(), mBarcodeDetector)
                .setRequestedPreviewSize(640, 480)
                .build();

        mCameraPreview.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

                    // Request permission
                    ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.CAMERA}, REQUEST_CAMERA_PERMISSION_ID);
                    return;
                }

                try {

                    // Commançer la capture
                    mCameraSource.start(mCameraPreview.getHolder());

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

                mCameraSource.stop();

            }
        });


        mDetectionHandler = new Handler() {
            public void handleMessage(Message msg) {

                mHandler = new Handler();

                // Vérifier si l'appareil existe dans la BD
                AppareilExistTask appareilExistTask = new AppareilExistTask(mHandler, DecoderFragment.this, String.valueOf(mAppareil_id));
                appareilExistTask.start();

            }
        };


        return v;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION_ID:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    try {
                        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        mCameraSource.start(mCameraPreview.getHolder());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getContext(), "Permission Denied!", Toast.LENGTH_SHORT).show();
                }
        }
    }


    @Override
    public void onStart() {
        super.onStart();

        mBarcodeDetector.setProcessor(new Detector.Processor<Barcode>() {

            @Override
            public void release() {

                // Récupérer le contenu du QR code détectée
                DetectionTask detectionTask = new DetectionTask(mQrCodes);
                detectionTask.start();
            }

            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                mQrCodes = detections.getDetectedItems(); // valeurs detectés dans les Qr codes

                if (mQrCodes.size() != 0) {

                    mAppareil_id = Integer.parseInt(mQrCodes.valueAt(0).displayValue);

                    mBarcodeDetector.release();

                }
            }
        });
    }

    // cas où l'appareil existe
    @Override
    public void onSucceed() {

        Intent intent = new Intent(getContext(), AppareilDetailsActivity.class);
        intent.putExtra("appareil_id", mAppareil_id);
        startActivity(intent);
    }

    @Override
    public void onFail() {

        Snackbar.make(getView(), getContext().getResources().getString(R.string.appareil_not_found_msg), Snackbar.LENGTH_LONG)
                .setAction("Réessayer", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onStart();
                    }
                }).show();
    }


    public class DetectionTask extends Thread {

        SparseArray<Barcode> qrcodes;

        public DetectionTask(SparseArray<Barcode> qrcodes) {

            this.qrcodes = qrcodes;
        }

        public void run() {

            // vibrer comme signe de capture d'un QR code
            Vibrator vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);
            vibrator.vibrate(100);

            mDetectionHandler.sendEmptyMessage(0);

        }

    }


}
