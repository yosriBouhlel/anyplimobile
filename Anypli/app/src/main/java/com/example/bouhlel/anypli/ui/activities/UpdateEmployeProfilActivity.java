package com.example.bouhlel.anypli.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.bouhlel.anypli.R;
import com.example.bouhlel.anypli.models.Employe;
import com.example.bouhlel.anypli.tasks.UpdateEmployeProfilTask;

import java.util.HashMap;

public class UpdateEmployeProfilActivity extends AppCompatActivity implements UpdateEmployeProfilTask.UpdateEmployeProfilListener {

    CollapsingToolbarLayout mToolbarLayout;
    private EditText mNom;
    private EditText mPrenom;
    private EditText mSpecialite;
    private EditText mEmail;
    private EditText mTel;
    private Button mModifier;
    private Handler mHandler;
    private HashMap<String, String> mParams;

    private Employe mEmploye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_employe_profil);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        mEmploye = (Employe) bundle.getSerializable("employe");

        bindActivity();

        mToolbarLayout.setTitle(mEmploye.getNom() + " " + mEmploye.getPrenom());
        mNom.setText(mEmploye.getNom());
        mPrenom.setText(mEmploye.getPrenom());
        mEmail.setText(mEmploye.getEmail());
        mSpecialite.setText(mEmploye.getSpecialite());
        mTel.setText(mEmploye.getTel());

        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/DroidSerif-BoldItalic.ttf");
        mModifier.setTypeface(face);

        mParams = new HashMap<>();

        mModifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mHandler = new Handler();

                if (validate()) {

                    mParams.put("employe_id", String.valueOf(mEmploye.getEmploye_id()));
                    mParams.put("nom", String.valueOf(mNom.getText().toString()));
                    mParams.put("prenom", String.valueOf(mPrenom.getText().toString()));
                    mParams.put("email", String.valueOf(mEmail.getText().toString()));
                    mParams.put("specialite", String.valueOf(mSpecialite.getText().toString()));
                    mParams.put("tel", String.valueOf(mTel.getText().toString()));

                    // Mettre à jour le profil de l'employé
                    UpdateEmployeProfilTask updateProfilTask = new UpdateEmployeProfilTask(mHandler, UpdateEmployeProfilActivity.this, mParams);
                    updateProfilTask.start();

                }
            }
        });

    }

    private void bindActivity() {
        mNom = (EditText) findViewById(R.id.employe_update_profil_nom_id);
        mPrenom = (EditText) findViewById(R.id.employe_update_profil_prenom_id);
        mSpecialite = (EditText) findViewById(R.id.employe_update_profil_specialite_id);
        mEmail = (EditText) findViewById(R.id.employe_update_profil_email_id);
        mTel = (EditText) findViewById(R.id.employe_update_profil_tel_id);
        mModifier = (Button) findViewById(R.id.modifier_employe_btn_id);
        mToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
    }

    // Hide virtual keyboard when touch out side EditText
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        View view = getCurrentFocus();
        boolean ret = super.dispatchTouchEvent(event);

        if (view instanceof EditText) {
            View w = getCurrentFocus();
            int scrcoords[] = new int[2];
            w.getLocationOnScreen(scrcoords);
            float x = event.getRawX() + w.getLeft() - scrcoords[0];
            float y = event.getRawY() + w.getTop() - scrcoords[1];

            if (event.getAction() == MotionEvent.ACTION_UP
                    && (x < w.getLeft() || x >= w.getRight()
                    || y < w.getTop() || y > w.getBottom())) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
            }
        }
        return ret;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home: {
                setResult(RESULT_CANCELED);
                finish();
                return false;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // vérifier que les infos saisies sont valides
    public boolean validate() {
        boolean valid = true;

        String email = mEmail.getText().toString();
        String tel = mTel.getText().toString();
        String specialite = mSpecialite.getText().toString();
        String nom = mNom.getText().toString();
        String prenom = mPrenom.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmail.setError(getResources().getString(R.string.sign_up_email_message));
            valid = false;
        } else {
            mEmail.setError(null);
        }

        if (nom.isEmpty() || nom.length() < 3) {
            mNom.setError(getResources().getString(R.string.sign_up_name_message));
            valid = false;
        } else {
            mNom.setError(null);
        }

        if (prenom.isEmpty() || prenom.length() < 3) {
            mPrenom.setError(getResources().getString(R.string
                    .sign_up_name_message));
            valid = false;
        } else {
            mPrenom.setError(null);
        }

        if (specialite.isEmpty() || specialite.length() < 3) {
            mSpecialite.setError(getResources().getString(R.string
                    .sign_up_name_message));
            valid = false;
        } else {
            mSpecialite.setError(null);
        }

        if (!Patterns.PHONE.matcher(tel).matches() || tel.length() < 8) {
            mTel.setError(getResources().getString(R.string.phone_number_message));
            valid = false;
        } else {
            mTel.setError(null);
        }


        return valid;
    }


    // modification du profil de l'employé terminé avec succée
    @Override
    public void onUpdateSuccess() {
        Toast.makeText(UpdateEmployeProfilActivity.this, getResources().getString(R.string.update_user_success_msg), Toast.LENGTH_LONG).show();

        mEmploye.setNom(mNom.getText().toString());
        mEmploye.setPrenom(mPrenom.getText().toString());
        mEmploye.setEmail(mEmail.getText().toString());
        mEmploye.setSpecialite(mSpecialite.getText().toString());
        mEmploye.setTel(mTel.getText().toString());

        Intent backToProfil = new Intent();
        backToProfil.putExtra("employe", mEmploye);
        setResult(RESULT_OK, backToProfil);
        finish();
    }

    @Override
    public void onUpdateFail() {
        Toast.makeText(UpdateEmployeProfilActivity.this, getResources().getString(R.string.update_user_fail_msg), Toast.LENGTH_LONG).show();
    }
}
